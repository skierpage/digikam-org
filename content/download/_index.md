---
date: "2017-03-21"
title: "Download"
author: "digiKam Team"
description: "Where to obtain digiKam."
category: "download"
aliases: "/download"
---


<!-- Before you can use digiKam, you must first download and install it. There are many ways to achieve that: -->

<!-- ### Official Bundles -->

<!-- This is the easiest, and recommended, way to get the most recent stable version. -->
<!-- This includes a universal Linux AppImage bundle, macOS package, and Windows Installer. -->

<!-- You can find the bundles here: http://download.kde.org/stable/digikam/ -->

### Package By Distribution

[Install a package](/download/binary/) that is made available through your distribution.

Keep in mind that distro-provided versions of digiKam could be out of date.
macOS and Windows users should refer to the [official bundles](#official-bundles) listed above.

Note: to integrate the AppImage bundle in Linux desktop, you can use the [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher) application.


### Pre-Releases

These are betas and release-candidates provided by the team for testing.

These should be more stable than weekly snapshots and testing will help to ready them for release.
This includes a universal Linux AppImage, macOS package, and Windows installer.

[Download the latest pre-release](https://download.kde.org/unstable/digikam/).


### Weekly Snapshots

These are compiled weekly versions provided by the team for testing purposes only.

This is the easiest way to try the current implementation that hasn't been released yet.
This includes a universal Linux AppImage, macOS package, and Windows installer.

[Download the latest weekly snapshots](https://files.kde.org/digikam/).


### Compile the Official Release

This is an option for those that are comfortable with compiling the project themselves.

Get the source for the [latest official release](http://download.kde.org/stable/digikam/) and build.


### Compile Latest Development Version

This is more complicated, but is as recent as possible.
This is for when you want to help with finding bugs or to hack on the project as a contributor.

Get the latest code [from our git repository](/download/git/).

You can choose the one which fits you best. We provide detailed instructions for each of them.
