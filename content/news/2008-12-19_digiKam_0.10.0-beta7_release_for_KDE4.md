---
date: "2008-12-19T12:59:00Z"
title: "digiKam 0.10.0-beta7 release for KDE4"
author: "digiKam"
description: "Dear all digiKam fans and users! After a huge bug triage where more than 120 bug reports have been closed since 0.10.0-beta6 release, digiKam development"
category: "news"
aliases: "/node/416"

---

<p>Dear all digiKam fans and users!</p>


<a href="http://www.flickr.com/photos/digikam/3117877562/" title="lighttable-kde4 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3102/3117877562_31b47ea2d6.jpg" width="500" height="400" alt="lighttable-kde4"></a>

<p>After a huge <a href="http://www.digikam.org/node/411">bug triage</a> where more than <b>120 bug reports have been closed</b> since <a href="http://www.digikam.org/node/409">0.10.0-beta6 release</a>, digiKam development team is happy to release the 7th beta dedicated to KDE4 desktop. The digiKam tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a>.</p>

<p>Following <a href="http://www.digikam.org/about/releaseplan">release plan</a>, another beta release will be done next year. We need volunteers to test again and again this version to be able to provide the best release for KDE 4.2. Please time some minutes during your Chrismast holidays to test digiKam and report. Thanks in advance...</p>
 
<p>digiKam is also compilable under Windows using MinGW and Microsoft Visual C++. Precompiled packages are available with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>To compile digiKam for KDE4, you need <b>libkexiv2</b>, <b>libkdcraw</b>, and <b>libkipi</b> for KDE4. These libraries are now included in KDE core but for the moment, digiKam require libkdcraw and libkexiv2 from svn trunk to compile fine (These versions of libraries will be released with KDE 4.2). To extract libraries source code from svn, look at bottom of <a href="http://www.digikam.org/download?q=download/svn">this page</a> for details</p>

<p>For others depencies, consult the <a href="http://websvn.kde.org/trunk/extragear/graphics/digikam/README?view=markup">README file</a>. There are also optional depencies to enable some new features as lensfun for lens auto-correction, and marble for geolocation.</p>
 
<p>The digikam.org web site has been redesigned for the new KDE4 series. All <a href=" http://www.digikam.org/node/323">screenshots</a> have been updated and a lots of <a href="  http://www.digikam.org/tour">video demos</a> have been added to present new features included in this version.</p>
 
<h5>NEW FEATURES (since 0.9.x series):</h5><br>

<b>General</b> : Ported to CMake/Qt4/KDE4.<br>
<b>General</b> : Thumbs KIO-Slave removed. digiKam now use multi-threading to generate thumnails.<br>
<b>General</b> : Removed all X11 library dependencies. Code is now portable under MACOS-X and Win32.<br>
<b>General</b> : Support of XMP metadata (require <a href="http://www.exiv2.org">Exiv2 library</a> &gt;= 0.16).<br>
<b>General</b> : Hardware handling using KDE4 Solid interface.<br>
<b>General</b> : Preview of Video and Audio files using KDE4 Phonon interface.<br>
<b>General</b> : Database file can be stored on a customized place to support remote album library path.<br>
<b>General</b> : New database schema to host more photo and collection informations.<br>
<b>General</b> : Database interface fully re-written using Qt4 SQL plugin.<br>
<b>General</b> : Support of multiple roots album paths.<br>
<b>General</b> : Physical root albums are managed as real album.<br>
<b>General</b> : New option in Help menu to list all RAW file formats supported.<br>
<b>General</b> : Geolocation of pictures from sidebars is now delegate to KDE4 Marble widget.<br>
<b>General</b> : New option in Help menu to list all main components/libraries used by digiKam.<br>
<b>General</b> : libkdcraw dependency updated to 0.4.0.<br>
<b>General</b> : libkexiv2 dependency updated to 0.5.0.<br>
<b>General</b> : Raw metadata can be edited, changed, added to TIFF/EP like RAW file formats (require Exiv2 &gt;= 0.18). Currently DNG, NEF, and PEF raw files are supported. More will be added in the future.<br>
<b>General</b> : digiKam can be compiled natively under Microsoft Windows.<br>
<b>General</b> : libkdcraw dependency updated to 0.4.0.<br>
<b>General</b> : libgphoto2 dependency is now optional to be able to compile digiKam under operating systems not supported by GPhoto2.<br>
<b>General</b> : Usability improvements to be able to run digiKam on small screen (like EeePC).<br>
<b>General</b> : New option to enable RAW metadata writing.<br>
<b>General</b> : digiKamlinktree script updated to support new Database schema.<br><br>

<b>CameraGUI</b> : New design for camera interface.<br>
<b>CameraGUI</b> : New Capture tool.<br>
<b>CameraGUI</b> : New bargraph to display camera media free-space.<br><br>
 
<b>AlbumGUI</b> : Added Thumbbar with Preview mode to easy navigate between pictures.<br>
<b>AlbumGUI</b> : Integration of Simple Text Search tool to left sidebar as Amarok.<br>
<b>AlbumGUI</b> : New advanced Search tools. Re-design of Search backend, based on XML. Re-design of search dialog for a better usability. Searches based on metadata and image properties are now possible.<br>
<b>AlbumGUI</b> : New fuzzy Search tools based on sketch drawing template. Fuzzy searches backend use an Haar wevelet interface. You simply draw a rough sketch of what you want to find and digiKam displays for you a thumbnail view of the best matches.<br>
<b>AlbumGUI</b> : New Search tools based on marble widget to find pictures over a map.<br>
<b>AlbumGUI</b> : New Search tools to find similar images against a reference image.<br>
<b>AlbumGUI</b> : New Search tools to find duplicates images around whole collections.<br><br>
 
<b>ImageEditor</b> : Added Thumbbar to easy navigate between pictures.<br>
<b>ImageEditor</b> : New plugin based on <a href="http://lensfun.berlios.de">LensFun library</a> to correct automaticaly lens aberrations.<br>
<b>ImageEditor</b> : LensDistortion and AntiVignetting are now merged with LensFun plugin.<br>
<b>ImageEditor</b> : All image plugin tool settings provide default buttons to reset values.<br>
<b>ImageEditor</b> : New Raw import tool to handle Raw pictures with customized decoding settings.<br>
<b>ImageEditor</b> : All image plugin dialogs are removed. All tools are embedded in editor window.<br>

<h5>BUG FIXES:</h5><br>

001 ==&gt; 146864 : Lesser XMP support in digiKam.<br>
002 ==&gt; 145096 : Request: acquire mass storage from printer as from camera. Change menu "Camera" to "Acquire".<br>
003 ==&gt; 134206 : Rethink about: Iptc.Application2.Urgency digiKam Rating.<br>
004 ==&gt; 149966 : Alternative IPTC Keyword Separator (dot notation).<br>
005 ==&gt; 129437 : Album could point to network path. Now it's impossible to view photos from shared network drive.<br>
006 ==&gt; 137694 : Allow album pictures to be stored on network devices.<br>
007 ==&gt; 114682 : About library path.<br>
008 ==&gt; 122516 : Album Path cannot be on Network device (Unmounted).<br>
009 ==&gt; 107871 : Allow multiple album library path.<br>
010 ==&gt; 105645 : Impossible to not copy images in ~/Pictures.<br>
011 ==&gt; 132697 : Metadata list has no scrollbar.<br>
012 ==&gt; 148502 : Show rating in embedded preview / slideshow.<br>
013 ==&gt; 155408 : Thumbbar in the album view.<br>
014 ==&gt; 138290 : GPSSync plugin integration in the side bar.<br>
015 ==&gt; 098651 : Image Plugin filter based on clens.<br>
016 ==&gt; 147426 : Search for non-voted pics.<br>
017 ==&gt; 149555 : Always present search box instead of search by right-clicking and selecting simple or advanced search.<br>
018 ==&gt; 139283 : IPTC Caption comment in search function.<br>
019 ==&gt; 150265 : Avanced search filter is missing search in comment / description.<br>
020 ==&gt; 155735 : Make it possible to search on IPTC-text.<br>
021 ==&gt; 147636 : GUI error in advanced searches: lots of free space.<br>
022 ==&gt; 158866 : Advanced Search on Tags a mess.<br>
023 ==&gt; 149026 : Search including sub-albums.<br>
024 ==&gt; 153070 : Search for image by geo-location.<br>
025 ==&gt; 154764 : Pictures saved into root album folder are not shown.<br>
026 ==&gt; 162678 : digiKam crashed while loading.<br>
027 ==&gt; 104067 : Duplicate image finder should offer more actions on duplicate images found.<br>
028 ==&gt; 107095 : Double image removal: Use trashcan.<br>
029 ==&gt; 112473 : findimages shows only small thumbnails.<br>
030 ==&gt; 150077 : Find Duplicate Images tool quite unusable on many images (a couple of issues).<br>
031 ==&gt; 161858 : Find Duplicate Image fails with Canon Raw Files.<br>
032 ==&gt; 162152 : Batch Duplicate Image Management.<br>
033 ==&gt; 164418 : GPS window zoom possibility.<br>
034 ==&gt; 117287 : Search albums on read only album path.<br>
035 ==&gt; 164600 : No picture in view pane.<br>
036 ==&gt; 164973 : Showfoto crashed at startup.<br>
037 ==&gt; 165275 : build-failure - imageresize.cpp - 'KToolInvocation' has not been declared.<br>
038 ==&gt; 165292 : albumwidgetstack.cpp can't find Phonon/MediaObject.<br>
039 ==&gt; 165341 : Crash when changing histogram channel when welcome page is shown.<br>
040 ==&gt; 165318 : digiKam doesn't start because of SQL.<br>
041 ==&gt; 165342 : Crash when changing album sort modus.<br>
042 ==&gt; 165338 : Right sidebar initally too big.<br>
043 ==&gt; 165280 : Sqlite2 component build failure.<br>
044 ==&gt; 165769 : adjustcurves.cpp - can't find version.h.<br>
045 ==&gt; 166472 : Thumbnail bar gone in image editor when switching back from fullscreen.<br>
046 ==&gt; 166663 : Tags not showing pictures without "Include Tag Subtree".<br>
047 ==&gt; 166616 : Filmstrip mode in View.<br>
048 ==&gt; 165885 : Thumbnails and images are NOT displayed in the main view center pane.<br>
049 ==&gt; 167139 : Crash if Exif.GPSInfo.GPSAltitude is empty.<br>
050 ==&gt; 167168 : Timeline view shows redundant extra sections.<br>
051 ==&gt; 166440 : Removing images from Light Table is not working properly.<br>
052 ==&gt; 166484 : digiKam crashes when changing some settings and using "find similar" after the changes made.<br>
053 ==&gt; 167124 : Timeline view not updated when changing selected time range.<br>
054 ==&gt; 166564 : Display of *already* *created* thumbnails is slow.<br>
055 ==&gt; 166483 : Error in "not enough disk space" error message.<br>
056 ==&gt; 167379 : Image selection for export plugin.<br>
057 ==&gt; 166622 : Confusing Add Images use.<br>
058 ==&gt; 166576 : digiKam quits when running "Rebuild all Thumbnail Images" twice.<br>
059 ==&gt; 167562 : Image Editor Shortcut Keys under Edit redo/undo are missing in GUI.<br>
060 ==&gt; 167561 : Crash when moving albums.<br>
061 ==&gt; 167529 : Image Editor not working correctly after setting "use horizontal thumbbar" option.<br>
062 ==&gt; 167621 : digiKam crashes when trying to remove a tag with the context menu.<br>
063 ==&gt; 165348 : Problems when trying to import KDE3 data.<br>
064 ==&gt; 166424 : Crash when editing Caption with Digikam4 SVN.<br>
065 ==&gt; 166310 : Preview not working in image effect dialogs.<br>
066 ==&gt; 167571 : Unnatural order when removing images from light table.<br>
067 ==&gt; 167139 : Crash if Exif.GPSInfo.GPSAltitude is empty.<br>
068 ==&gt; 167778 : Assigning ratings in image editor via shortcuts or toolbar not working.<br>
069 ==&gt; 168567 : Unable to edit or remove iptc and xmp info.<br>
070 ==&gt; 168846 : crash after playing with tags (seems to be rather a crash in Qt or kdelibs).<br>
071 ==&gt; 166671 : Image filter dialog buttons are much too big.<br>
072 ==&gt; 134486 : Keywords are not written to raw files even though they do embed iptc/exif.<br>
073 ==&gt; 168839 : Digikam crashed after tagging.<br>
074 ==&gt; 167085 : Color selection in sketch search tool looks plain black.<br>
075 ==&gt; 168852 : Crash on profile application.<br>
076 ==&gt; 167867 : Album view is reset by changing settings.<br>
077 ==&gt; 168461 : Info in Properties panel not updated after moving to other image.<br>
078 ==&gt; 169704 : Crash during RAW import.<br>
079 ==&gt; 166437 : Deleting images in Image Editor not working properly.<br>
080 ==&gt; 169814 : Compilation error: no exp2().<br>
081 ==&gt; 165345 : Selecting images from bottom right to top left does not work.<br>
082 ==&gt; 170693 : Enable geotagging of Olympus orf images.<br>
083 ==&gt; 170711 : Write single Keywords instead of Hirachy into IPTC.<br>
084 ==&gt; 168569 : New Album Folders are not detected in displayed in GUI until restart.<br>
085 ==&gt; 165337 : Text overlapping box on welcome page.<br>
086 ==&gt; 171310 : No preview photos shown clicking on imported albums.<br>
087 ==&gt; 171778 : digiKam does not load photos in its database.<br>
088 ==&gt; 171736 : Make transferring of metadata between machines easier.<br>
089 ==&gt; 170929 : Compilation of SVN version fails with gcc 4.3.2 unless debug option is activated.<br>
090 ==&gt; 171989 : Can't set gps photo location on .nef images.<br>
091 ==&gt; 172018 : Initial cursor in the "sharpen" dialog is on the "cancel" button.<br>
092 ==&gt; 172033 : Unable to draw rectangle from right to left.<br>
093 ==&gt; 171953 : Using create "Date-based sub-albums" makes import fail.<br>
094 ==&gt; 118318 : Make (lib)gphoto support a configure flag.<br>
095 ==&gt; 172269 : digiKam hangs for around 20-60 seconds when starting.<br>
096 ==&gt; 172648 : Thumbnail click does not display embedded picture preview.<br>
097 ==&gt; 172650 : No kipi plugins available.<br>
098 ==&gt; 172651 : Send image by email missing.<br>
099 ==&gt; 168005 : Week view broken in timeline.<br>
100 ==&gt; 172295 : digiKam has a hard dependency on Jasper library.<br>
101 ==&gt; 172733 : Crash on Synchronize images with database.<br>
102 ==&gt; 172776 : Combine Edit and Album menus, and other ideas.<br>
103 ==&gt; 172836 : No menu item for lensfun.<br>
104 ==&gt; 172912 : "Slide" becomes "Slideshow" text on button in Main window.<br>
105 ==&gt; 172882 : Setup verbiage patches.<br>
106 ==&gt; 172884 : Welcome/First-Run verbiage patches.<br>
107 ==&gt; 172892 : Tips verbiage changes.<br>
108 ==&gt; 172895 : Setup verbiage patches for slideshow and albumview.<br>
109 ==&gt; 172898 : Verbiage changes for Util-ImageEditor-ImageResize.cpp.<br>
110 ==&gt; 172899 : Verbiage changes for digikamapp.cpp.<br>
111 ==&gt; 167320 : Filmstrip in album view is not synchronized correctly.<br>
112 ==&gt; 172911 : Album File lister does not have an "All Image Files" filter.<br>
113 ==&gt; 167777 : Thumbnails in Album not shown at first start of digiKam.<br>
114 ==&gt; 173645 : digiKam crashes after start dialog.<br>
115 ==&gt; 173691 : digiKam doesn't show pictures.<br>
116 ==&gt; 091811 : Feature request: find pictures by meta tag info, date.<br>
117 ==&gt; 173853 : seg fault digiKam with marble widget (at startup).<br>
118 ==&gt; 173995 : digiKam crashes on loading main view.<br>
119 ==&gt; 163143 : digiKam doesn't detect new photos in import from a camera.<br>
120 ==&gt; 107316 : Option: Only download new photos from camera.<br>
121 ==&gt; 174108 : digikam and exiv2 crash with AVI file.<br>
122 ==&gt; 174202 : Albums` names are duplicated after migrating from digikam-0.9.4.<br>
123 ==&gt; 167873 : Tag and album trees are not expanded when searching for entries.<br>
124 ==&gt; 169641 : digiKam crashed while downloading AVI-files from Canon S5 IS.<br>
125 ==&gt; 169964 : Crash when importing my picture and video collection.<br>
126 ==&gt; 174615 : digiKam 0.10 compile error.<br>
127 ==&gt; 174620 : After updating to current svn digikam crashes.<br>
128 ==&gt; 175650 : digiKam crash at startup without Phonon problems.<br>
129 ==&gt; 173918 : digiKam msvc binaries doesn't work.<br>
130 ==&gt; 175662 : USB camera transfer hangs and works only the second time tried.<br>
131 ==&gt; 174991 : digiKam freezes when generating new tag.<br>
132 ==&gt; 171414 : Crash (with SIGSEV) when closing the application.<br>
133 ==&gt; 175597 : Crash when trying to access usb cam (autodetected).<br>
134 ==&gt; 176023 : digiKam svn: compile error.<br>
135 ==&gt; 174752 : Creating a new album gives _two_ new albums, with the same name.<br>
136 ==&gt; 172680 : digiKam crashes when I try to download the photos from my camera.<br>
137 ==&gt; 166578 : Default Keyboard zoom requires three fingers.<br>
138 ==&gt; 171318 : digiKam backend does not exit on Quit - processes hang.<br>
139 ==&gt; 175401 : digiKam doesn't always detect mass storage camera.<br>
140 ==&gt; 176472 : Moving/Deleting all files from an album crashes digiKam.<br>
141 ==&gt; 165885 : Thumbnails and images are not displayed in the main view center pane.<br>
142 ==&gt; 166439 : Drag and drop on lighttable does not work.<br>
143 ==&gt; 172730 : Preview only, but no photo details shown at all.<br>
144 ==&gt; 169830 : digiKam crashed when navigating large images.<br>
145 ==&gt; 173533 : Crash when deselecting a single photo with CTRL+left mouse click.<br>
146 ==&gt; 169764 : RAW file import lacks I18N support, no decoding.<br>
147 ==&gt; 168918 : Rectangular selection tool often looses selection when dragging vertices.<br>
148 ==&gt; 176654 : Buttons for setting album date in album properties not working.<br>
149 ==&gt; 173746 : Embed view does not work; I can not see an image.<br>
150 ==&gt; 172164 : Crash when downloading photos from Canon Powershot A95.<br>
151 ==&gt; 165325 : Crash when trying to import pictures from a USB-Camera.<br>
152 ==&gt; 174642 : Crashes when try to connect to Kodak Z812 IS.<br>
153 ==&gt; 168514 : Minimum Screen Size Too Large.<br>
154 ==&gt; 133058 : Kodak DC4800 can only be read if KDE did not try to mount it.<br>
155 ==&gt; 128783 : HP Instant Share to work with digiKam.<br>
156 ==&gt; 132064 : Showing SVG / Vector Graphics.<br>
157 ==&gt; 138548 : Error downloading or detecting camera when connected.<br>
158 ==&gt; 137233 : Proper input constraints in New Album dialog.<br>
159 ==&gt; 176086 : Recursive slideshow only shows last album.<br>
160 ==&gt; 171234 : Tag names imported from KDE3 digiKam are cut off in KDE4.<br>
161 ==&gt; 173828 : Geolocation do not except Canon EOS 1D Mark III CR2-files.<br>
162 ==&gt; 150938 : Album thumbnail view very slow on NFS.<br>
163 ==&gt; 137707 : A new icon for the albums in digiKam.<br>
164 ==&gt; 176725 : Drag and drop stopped working.<br>
165 ==&gt; 121460 : Album date average incorrect.<br>
166 ==&gt; 115046 : Light-table usability, possible improvements.<br>
167 ==&gt; 176584 : Mousewheeling on a slideshow sometimes crash digiKam.<br>
168 ==&gt; 147477 : Camera &gt; Media Browse does not populate information until click second time.<br>
169 ==&gt; 169228 : View and Edit Icons missing in medium size.<br>
170 ==&gt; 165321 : Maximized mode doesn't work, digiKam is "under" the taskbar.<br>
171 ==&gt; 175914 : Select new items in camera import window selects all files / forgets status.<br>
172 ==&gt; 132362 : Support for BOTH "jpgComment" and "jpgUser comment" EXIF data.<br>
173 ==&gt; 133878 : Filter which files to import.<br>
174 ==&gt; 167874 : Item count in tag and album view is not updated.<br>
175 ==&gt; 176972 : Using create "Date-based  sub-albums"  makes import fail.<br>
176 ==&gt; 131447 : Problem dragging folders from digiKam to other places, like the desktop.<br>
177 ==&gt; 156847 : Missing thumbnails in album overview.<br>
178 ==&gt; 146714 : IE, showFoto, album view rotation: rotated TIFs are losing metadata about picture.<br>
179 ==&gt; 147961 : Search / Order by color.<br>
180 ==&gt; 151922 : Assign-tag menu icons sometimes in the wrong place.<br>
181 ==&gt; 176565 : Loading a directory with more than 5000 raw files (NEF) make digiKam crash.<br>
182 ==&gt; 152262 : Unable to load tiff file.<br>
183 ==&gt; 148779 : Resync or refresh IPTC-comments.<br>
184 ==&gt; 147772 : Confusing UI for re-sizing side-bars.<br>
185 ==&gt; 098225 : Printing and displaying Exif information over the picture.<br>
186 ==&gt; 171953 : Using create "Date-based  sub-albums"  makes import fail.<br>
187 ==&gt; 133211 : Media browse corrupted display.<br>
188 ==&gt; 168928 : Rotation of raw images.<br>
189 ==&gt; 174683 : Crashes when try to correct red eye.<br>
190 ==&gt; 136206 : Losing tags after an move or rename of image files.<br>
191 ==&gt; 142774 : Moving pictures between albums.<br>
192 ==&gt; 160664 : Protected file to camera.<br>
193 ==&gt; 143864 : Tool to remove Chromatic Aberration from photos.<br>
194 ==&gt; 125233 : "purple fringe" correction plugin.<br>
195 ==&gt; 150161 : UFRaw as plugin for RAW.<br>
196 ==&gt; 147876 : Broken/renamed images after tagging.<br>
197 ==&gt; 117629 : Do an OCR of an image and store result in Exif/jfif.<br>
198 ==&gt; 172835 : The stars of the rating in the right panel are not refreshed when selecting different images.<br>
199 ==&gt; 153798 : Bad calculation of images in recursive showed albums.<br>
200 ==&gt; 134679 : Video and audio files are not imported.<br>
201 ==&gt; 171480 : Visual indicator missing for 100% zoom level.<br>
202 ==&gt; 151861 : Canon Powershot 200 is not recognized in this final version.<br>
203 ==&gt; 144215 : Change the comment of a picture without modify its timestamp.<br>
204 ==&gt; 150609 : Adjust Exif orientation tag changes files date.<br>
205 ==&gt; 157788 : Tools-&gt;Update Metadata Database changes timestamps of files.<br>
206 ==&gt; 142568 : digiKam is really slow to load a RAW (.CR2) photo.<br>
207 ==&gt; 151719 : Non latin1 in IPTC keywords.<br>
208 ==&gt; 132244 : Special Chars in Keywords decode wrong in IPTC.<br>
209 ==&gt; 162490 : dcraw-8.86 adds newer canon dslr with digic III support.<br>
210 ==&gt; 103149 : Printing wizard plugin is hard to find.<br>
211 ==&gt; 149328 : Let libkdcraw use raw decoding options when generating thumbnails.<br>
212 ==&gt; 168012 : Thumbnails not shown in find similar items tool so they can't be used for drag and drop.<br>
213 ==&gt; 148861 : [CTRL] + scroll wheel doesn't resize thumbnails.<br>
214 ==&gt; 154526 : Big screen size lead to small picture in main window and editor.<br>
215 ==&gt; 177231 : Rotate context menu in view mode.<br>
216 ==&gt; 125923 : Image tooltip contents not shown when albumview is scrolled up or down with cursor keys.<br>
217 ==&gt; 174807 : digiKam does not use KDE's Trash.<br>
218 ==&gt; 142058 : Icons for import should use proper mime type.<br>
219 ==&gt; 176179 : Image editor - zoom +/- buttons alter predefined list of zoom factors.<br>
220 ==&gt; 177270 : Making OpenStreetMap available in the GPS-Sidebar.<br>
221 ==&gt; 177236 : No tooltips for cut elements in metadata panel.<br>
222 ==&gt; 177185 : Status bar position of current image starts from 0.<br>
223 ==&gt; 172632 : Album item tooltip doesn't use colors of themes properly.<br>
224 ==&gt; 173746 : digiKam doesn't show "big view".<br>
225 ==&gt; 177093 : User should not be able to delete root album.<br>
226 ==&gt; 177333 : digiKam can't find images in album.<br>
227 ==&gt; 149360 : Not all menu items may be bound to keyboard shortcuts.<br>
228 ==&gt; 167838 : File details panel disappears.<br>
229 ==&gt; 173790 : Edit previews happen too soon in Showfoto.<br>
230 ==&gt; 148892 : Showfoto shows images in very poor resolution.<br>
231 ==&gt; 177327 : Long tooltips are cut in metadata panel.<br>
232 ==&gt; 170784 : Metadata XMP Tab Simple List should show more metadata.<br>
233 ==&gt; 177422 : Crash after trying to overwrite image.<br>
234 ==&gt; 175322 : Import from camera: unable to create new folders.<br>
235 ==&gt; 177250 : Crash when importing pictures from Nikon D200 PTP.<br>
236 ==&gt; 161304 : Status bar info cleared after rating assigment.<br>
237 ==&gt; 149875 : Support interlaced PNG images.<br>
238 ==&gt; 177457 : digiKam crash when tagging files.<br>
239 ==&gt; 177608 : Wrong button text to save image tags and caption.<br>
240 ==&gt; 141601 : File size not updated after inserting Metadata.<br>
241 ==&gt; 177396 : digiKam crashes when I open RAW (CR2) file in editor.<br>
242 ==&gt; 177888 : Caption text area is too large.<br>
243 ==&gt; 146618 : Media viewer does not sort correctly.<br>
244 ==&gt; 156338 : DPS Files appearing in capture images dialog.<br>
245 ==&gt; 133185 : Better support of JVC camera : mgr_data and prg_mgr files are not images but are available to download.<br>
246 ==&gt; 154941 : Improve exception safety with smart pointers.<br>
247 ==&gt; 177894 : digiKam not displaying images.<br>
248 ==&gt; 175925 : USB Storage size (and Local disk) are wrong.<br>
249 ==&gt; 177933 : Viewing image doesn't work.<br>
250 ==&gt; 168004 : Timeline gives zero height column for dates with a single entry.<br>
251 ==&gt; 176231 : Browse Media Menu Not Populated.<br>
252 ==&gt; 177953 : digiKam crash when saving current search.<br>
253 ==&gt; 158565 : digiKam ignores files created through external applications like gimp.<br>
254 ==&gt; 177242 : mtime ctime rename exif import.<br>
255 ==&gt; 151275 : Camera tool: date &amp; time should be taken from exif not file modified date.<br>
256 ==&gt; 146764 : Media viewer sort images by date and try to fill videos without exiv in the correct order (by filename).<br>
257 ==&gt; 175898 : Selecting in the import dialog is extremly slow.<br>

<p>digiKam team wish you a Merry Christmas and an happy new year.</p>


<div class="legacy-comments">

  <a id="comment-18079"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/416#comment-18079" class="active">Isn't it "Bug Fixes" instead</a></h3>    <div class="submitted">Submitted by blueget (not verified) on Fri, 2008-12-19 18:23.</div>
    <div class="content">
     <p>Isn't it "Bug Fixes" instead of "Bugs Fixes"? I think "Bugs Fixed" is also possible, but I'm quite sure that "Bugs Fixes" is wrong.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18081"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/416#comment-18081" class="active">Thank you very much for the</a></h3>    <div class="submitted">Submitted by blueget (not verified) on Fri, 2008-12-19 19:28.</div>
    <div class="content">
     <p>Thank you very much for the fast correction! (you can remove my comments if you wish)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18082"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/416#comment-18082" class="active">Sweet, like honey. :)</a></h3>    <div class="submitted">Submitted by <a href="http://kubuntulover.blogspot.com" rel="nofollow">Bugsbane</a> (not verified) on Fri, 2008-12-19 19:35.</div>
    <div class="content">
     <p>Way to go Gilles!</p>
<p>digiKam's looking better than ever from the screenshots. You said that some of the dependencies will be included with KDE 4.2. Do you know if they're already included with 4.2 beta 2? I so want to help test DigiKam out, but previous attempts at compiling all failed. :(</p>
<p>Anyway, awesome work as usual. Feel free to make all the "bugs fixes" you wish. I for one will not complain. (Mon Francais c'est tres, tres terrible!) :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18083"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/416#comment-18083" class="active">yes KDE 4.2 beta2 is fine normaly</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2008-12-19 19:57.</div>
    <div class="content">
     <p>libkexiv2 and libkdcraw have been frozen just before KDE 4.2-beta1. So normaly, all will be fine with beta2.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18088"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/416#comment-18088" class="active">Kubuntu packages</a></h3>    <div class="submitted">Submitted by <a href="http://patel.sh" rel="nofollow">dhuv</a> (not verified) on Mon, 2008-12-22 07:03.</div>
    <div class="content">
     <p>Now that Kubuntu has Beta2 packages for Intrepid, do you think it would be possible to get Digikam Beta packages for Kubuntu 8.10 starting from Beta7?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18089"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/416#comment-18089" class="active">intrepid 4.2 beta 2 + digikam</a></h3>    <div class="submitted">Submitted by Tim Middleton (not verified) on Mon, 2008-12-22 16:20.</div>
    <div class="content">
     <p>I'm running Kubuntu Intrepid with KDE 4.2 beta 2, and finally gave up waiting for Digikam 0.10 to appear... i found these extremely simple instructions which worked perfectly for me... and i'm much happier now.</p>
<p><a href="http://wiki.kde.org/tiki-index.php?page=Digikam+Compilation+on+Kubuntu+Intrepid">http://wiki.kde.org/tiki-index.php?page=Digikam+Compilation+on+Kubuntu+Intrepid</a></p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18093"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/416#comment-18093" class="active">thanks</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2008-12-30 15:12.</div>
    <div class="content">
     <p>Thanks for the link, worked fine with kde 4.2 beta 2 here as well!<br>
Also thanks for an excellent app!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18094"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/416#comment-18094" class="active">Hello
I am glad to see that</a></h3>    <div class="submitted">Submitted by Frederic (not verified) on Tue, 2008-12-30 18:16.</div>
    <div class="content">
     <p>Hello</p>
<p>I am glad to see that all my digikam 3 database has not been lost and is now properly translated in the new format.<br>
This new release is full of promises.<br>
Just a silly question : how to report bugs found in this beta release ?</p>
<p>Regards</p>
<p>Frederic</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18098"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/416#comment-18098" class="active">Look in Contribute page</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-01-05 11:52.</div>
    <div class="content">
     <p>&gt;how to report bugs found in this beta release ?</p>
<p>Follow instructions <a href="http://www.digikam.org/contrib">here</a></p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
