---
date: "2009-04-28T20:46:00Z"
title: "digiKam digest - 2009-04-26"
author: "Anonymous"
description: "This week digiKam saw introduction of new plugin - Liguid resize; beginning of work on Qt4 list view implementation and usual share of polishing, bug"
category: "news"
aliases: "/node/441"

---

<p><code></code></p>
<p>This week digiKam saw introduction of new plugin - Liguid resize; beginning of work on Qt4 list view implementation and usual share of polishing, bug fixing, etc. Read details in full version.</p>
<p>Bug/wish count</p>
<p>digikam       1:202 2:+11 3:-3 4:8 5:249 6:+1 7:-0 8:1<br>
kipiplugins   1:99 2:+4 3:-1 4:3 5:136 6:+1 7:-1 8:0       </p>
<p>[1] Opened bugs<br>
[2] Opened last week<br>
[3] Closed last week<br>
[4] Change<br>
[5] Opened wishes<br>
[6] Opened last week<br>
[7] Closed last week<br>
[8] Change</p>
<p>Full tables are here:<br>
<a href="https://bugs.kde.org/component-report.cgi?product=digikam">digiKam</a><br>
<a href="https://bugs.kde.org/component-report.cgi?product=kipiplugins">KIPI-plugins</a></p>
<p>-----------------------------------------------------------------------<br>
SVN commit 957437 by cgilles:</p>
<p>libkdcraw from trunk : OpenMP support : Parallelized RAW demosaicing<br>
operations are now enabled by default.  We check which GCC version is<br>
available to use right compilation/linking flags. If gcc version is bad,<br>
OpenMP support is disabled. </p>
<p>Note to packagers : a CMake option is available to disable OPenMP<br>
support if necessary (default=OFF).  Look in README for details.</p>
<p> M  +39 -12    CMakeLists.txt<br>
 M  +1 -0      NEWS<br>
 M  +5 -4      README<br>
 M  +3 -3      libkdcraw/kdcraw.cpp<br>
 M  +7 -2      libkdcraw/libraw_config.h.cmake<br>
-----------------------------------------------------------------------</p>
<p>SVN commit 958053 by cgilles:</p>
<p>apply patch #32985 from Matthias Welwarsky fo fix LensFun tool with<br>
Lanzcos method<br>
BUG: 186823</p>
<p> M  +7 -7      imageplugins/lenscorrection/autocorrection/klensfun.cpp<br>
 M  +1 -0      libs/dimg/dcolor.h<br>
 M  +16 -9     libs/dimg/dcolorblend.h<br>
 M  +218 -2    libs/dimg/dimg.cpp<br>
 M  +96 -80    libs/dimg/dimg.h<br>
 M  +28 -1     libs/dimg/dimgprivate.h<br>
-----------------------------------------------------------------------</p>
<p>===========================================<br>
Much work into content aware resizing tool</p>
<p><a href="https://bugs.kde.org/show_bug.cgi?id=149485"><br>
https://bugs.kde.org/show_bug.cgi?id=149485<br>
</a></p>
<p>More on Gilles blog:</p>
<p><a href="http://www.digikam.org/drupal/node/439"><br>
http://www.digikam.org/drupal/node/439<br>
</a></p>
<p>One of commits:<br>
===========================================</p>
<p>SVN commit 958077 by cgilles:<br>
move Liquid Rescale tool to digiKam core !!!</p>
<p> D            branches/work/~cgilles/contentawareresizing (directory)<br>
 A            trunk/extragear/graphics/digikam/imageplugins/contentawareresizing<br>
(directory) </p>
<p>-----------------------------------------------------------------------<br>
SVN commit 958769 by aclemens:</p>
<p>Remove the mapsearch tab from the left sidebar if marblewidget can not<br>
be found.<br>
Since we can have textual information in the geolocation tab in the<br>
right sidebar, I will not remove this one for now.</p>
<p> M  +33 -18    digikamview.cpp  </p>
<p>-----------------------------------------------------------------------<br>
===========================================<br>
Andi's description of this feature from mailing list:</p>
<p>It is since a few days. Before we had just disabled the menu entry. This<br>
was because we had not used the services architecture properly.  We just<br>
took the first selected image and decided what application can open<br>
these file types.<br>
But this is the wrong way to do it. If you select more then one image,<br>
chances are that you can not open all selected files in the application.<br>
So what we did was disabling the menu entry on multiple selection.<br>
Now we have a different approach (like dolphin and gwenview does, too).<br>
Generate a query for all selected mimetypes and provide applications<br>
that can handle all those mime types.<br>
If none can be found, open the "Open with" application dialog, where you<br>
can choose the app to work with by yourself.<br>
===========================================</p>
<p>SVN commit 958991 by aclemens:</p>
<p>Group the "Edit" and "Open With" actions together in the context menu of<br>
the albumiconview and imagepreviewview.<br>
They are related to each other and therefore should be in one place to<br>
easily find them.</p>
<p> M  +3 -1      albumiconview.cpp<br>
 M  +3 -1      imagepreviewview.cpp  </p>
<p>-----------------------------------------------------------------------<br>
===========================================<br>
Begin of work on Qt4 ListView implementation of digiKam icon view (aka<br>
Album GUI). This should create various new, better ways to present<br>
images and to manage them.</p>
<p>One of commits:<br>
===========================================</p>
<p>SVN commit 959105 by mwiesweg:</p>
<p>Working on various features:<br>
- tool tip support<br>
- QListView settings<br>
- watch flags for ImageModel<br>
- item activation handlers (need to check if single/double click<br>
  activation settings in Qt honour KDE settings)<br>
- methods to access current, selected and all image infos<br>
- scrollTo item support<br>
- context menu handler</p>
<p> M  +197 -7    imagecategorizedview.cpp<br>
 M  +32 -2     imagecategorizedview.h<br>
</p>

<div class="legacy-comments">

</div>