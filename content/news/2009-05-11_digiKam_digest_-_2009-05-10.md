---
date: "2009-05-11T16:06:00Z"
title: "digiKam digest - 2009-05-10"
author: "Anonymous"
description: "Usual portion of bug fixes and polishing. In addition: - main part of port to Qt4 ListModel view done - improving of Content Aware Resize"
category: "news"
aliases: "/node/448"

---

<p>Usual portion of bug fixes and polishing. In addition:</p>
<p>- main part of port to Qt4 ListModel view done<br>
- improving of Content Aware Resize plugin<br>
- polishing of Free Rotation interface<br>
- optimizations in thumbanils view<br>
- proof-reading of digiKam interface<br>
- introduction of tool to sanitize digiKam database from time to time<br>
- making sure everything compile on MS-Windows</p>
<p><code><br>
------------------------------------------------------------------------</code></p>
<p>Bug/wish count</p>
<p> digikam          223   +16     -4      12      255     +3      -1      2<br>
 kipiplugins      102   +4      -2      2       140     +4      -0      4</p>
<p>[1] Opened bugs<br>
[2] Opened last week<br>
[3] Closed last week<br>
[4] Change<br>
[5] Opened wishes<br>
[6] Opened last week<br>
[7] Closed last week<br>
[8] Change</p>
<p>Full tables:<br>
<a href="https://bugs.kde.org/component-report.cgi?product=digikam">digiKam</a><br>
<a href="https://bugs.kde.org/component-report.cgi?product=kipiplugins">KIPI-plugins</a></p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 963387 by mwiesweg:</p>
<p>Add method to create drag pixmap for view<br>
(concentrate all drawing code in the delegate)</p>
<p> M  +27 -0     imagedelegate.cpp<br>
 M  +2 -1      imagedelegate.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 963388 by mwiesweg:</p>
<p>- add custom startDrag method with pixmap drawn by delegate<br>
- when a file's content changes, redraw</p>
<p> M  +29 -1     imagecategorizedview.cpp<br>
 M  +2 -0      imagecategorizedview.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 963389 by mwiesweg:</p>
<p>Put all code from AlbumIconView that does actions with current or<br>
selected items and current album, but that does not really require to be<br>
in a view subclass, to a separate QObject class so that the code can be<br>
shared.<br>
The includes showing a delete dialog, adding to light table or queue<br>
manager, renaming an item, creating a new album for some items and<br>
changing an album thumbnail.</p>
<p> M  +1 -0      CMakeLists.txt<br>
 A             digikam/imageviewutilities.cpp   [License: GPL (v2+)]<br>
 A             digikam/imageviewutilities.h   [License: GPL (v2+)]</p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 963386 by mwiesweg:</p>
<p>Add copyright statements for copy+pasted code.<br>
Add signal to dispatch job result to widget's handling method</p>
<p> M  +3 -0      albumdragdrop.cpp<br>
 M  +8 -5      imagedragdrop.cpp<br>
 M  +3 -0      imagedragdrop.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 963956 by jnarboux:</p>
<p>Set side switch and enlargement steps as suggested by Carlo Baldassi.<br>
CCBUGS: 149485</p>
<p> M  +4 -0      contentawareresizer.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 964222 by aclemens:</p>
<p>Make the point circle slightly bigger. Still centering of the middle<br>
point fails. This is due to the fact that the circle is anti-aliased,<br>
but the point is not. If I use anti-aliasing for the point as well, it<br>
is perfectly centered, but looks crappy :-)<br>
So I guess current implementation is ok.</p>
<p> M  +4 -3      imageguidewidget.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 964154 by aclemens:</p>
<p>If the main angle of the rotation angle is zero, the rotation direction<br>
is wrongly detected, because<br>
int mainAngle = anglesList[0].toInt(&amp;ok);<br>
will convert "-0" to "0" and therefore the direction information might<br>
be lost.<br>
Use the unaltered angle instead...</p>
<p> M  +1 -1      freerotationtool.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 964240 by aclemens:</p>
<p>Don't enable the "Adjust" button if the points are equal. This is also<br>
checked by the calculateAngle method, but since it doesn't make sense to<br>
rotate in this case, disabling the button is more logical (also to the<br>
user).</p>
<p> M  +2 -1      freerotationtool.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 964646 by welwarsky:</p>
<p>move dummy border generation for refocus tool from sharpnesseditor to<br>
refocus filter refactor sharpnesseditor code accordingly</p>
<p> M  +17 -124   imageplugins/coreplugin/sharpnesseditor/sharpentool.cpp<br>
 M  +0 -2      imageplugins/coreplugin/sharpnesseditor/sharpentool.h<br>
 M  +71 -3     libs/dimg/filters/dimgrefocus.cpp<br>
 M  +2 -0      libs/dimg/filters/dimgrefocus.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 964603 by sengels:</p>
<p>fix thumbnailing:<br>
the problem was that KTemporaryFile kept the file in use, so renaming<br>
didn't work under Windows.  I fixed this bug already twice - the same<br>
code is spread all over KDE, so it might make sense if we can unify that<br>
using previewjob from<br>
kdelibs/kio.  BUG:189742</p>
<p> M  +10 -3     thumbnailcreator.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 964958 by jnarboux:</p>
<p>Add a method to set preview mode in plugins.<br>
Set mouse over preview mode for content aware resizing.<br>
Display mask only if original is displayed.</p>
<p>CCBUGS: 149485</p>
<p> M  +1 -0      imageplugins/contentawareresizing/contentawareresizetool.cpp<br>
 M  +6 -1      libs/widgets/imageplugins/imageguidewidget.cpp<br>
 M  +1 -0      libs/widgets/imageplugins/imageguidewidget.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 965424 by mwiesweg:</p>
<p>Split from searchutilities.cpp to libs/widgets/common.<br>
Adapt class names, signals to general use.<br>
Add keyboard navigation to click labels.<br>
Add EXPORT macros.</p>
<p> A             libs/widgets/common/clicklabel.cpp   [License: GPL (v2+)]<br>
 A             libs/widgets/common/clicklabel.h   [License: GPL (v2+)]<br>
 A             libs/widgets/common/visibilitycontroller.cpp   [License: GPL (v2+)]<br>
 A             libs/widgets/common/visibilitycontroller.h   [License: GPL (v2+)]<br>
 M  +0 -314    utilities/searchwindow/searchutilities.cpp<br>
 M  +0 -121    utilities/searchwindow/searchutilities.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 965527 by cgilles:</p>
<p>Temp file don not be deleted automatically. It's renamed just after...<br>
Tested under Linux. Look like thumbs is really faster now. Sound like we<br>
have this bug since KDE4 port is started (summer 2007)<br>
CCBUGS: 189742</p>
<p> M  +1 -4      thumbnailcreator.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 965716 by coles:</p>
<p>Proof-reading.</p>
<p> M  +8 -8      openfilepage.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 965590 by mwiesweg:</p>
<p>Split the combo box to select albums/tags from the SearchWindow to its<br>
own class.</p>
<p>CCMAIL: andi.clemens@gmx.net</p>
<p> M  +1 -0      CMakeLists.txt<br>
 A             digikam/albumselectcombobox.cpp   [License: GPL (v2+)]<br>
 A             digikam/albumselectcombobox.h   [License: GPL (v2+)]<br>
 M  +11 -43    utilities/searchwindow/searchfields.cpp<br>
 M  +3 -5      utilities/searchwindow/searchfields.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 965551 by welwarsky:</p>
<p>don't sprinkle code with #defines, DImgRefocus is the right place to<br>
store the max. matrix size fix small bug in sharpen batchtool (correct<br>
signal signature)</p>
<p> M  +1 -3      imageplugins/coreplugin/sharpnesseditor/sharpentool.cpp<br>
 M  +5 -0      libs/dimg/filters/dimgrefocus.cpp<br>
 M  +2 -0      libs/dimg/filters/dimgrefocus.h<br>
 M  +2 -4      utilities/queuemanager/basetools/enhance/sharpen.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 965812 by aclemens:</p>
<p>Fix little memory leak: the iterator object will not be destroyed<br>
automatically like other Qt objects. We need to delete it in the<br>
destructor.</p>
<p> M  +1 -0      batchprocessimagesdialog.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 965849 by aclemens:</p>
<p>This patch will make filtering of the albumiconview much faster,<br>
especially when displaying a big album recursively.</p>
<p>Marcel, Gilles,<br>
I will commit this little patch anyway now because I need it from time<br>
to time :-) Hopefully we don't need this with the upcoming ModelView<br>
infrastructure.</p>
<p> M  +1 -2      albumlister.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 965996 by aclemens:</p>
<p>First attempt of a man page for cleanup_digikamdb. Since the script is<br>
not doing much, I don't know what to put in there :-)</p>
<p> M  +1 -0      CMakeLists.txt<br>
 A             cleanup_digikamdb.1  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 965981 by aclemens:</p>
<p>This little script can be used to clean up ("VACUUM;") the digiKam DB.<br>
It will increase speed and reduce the size of the database.</p>
<p> AM            cleanup_digikamdb.sh  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 965982 by cgilles:</p>
<p>remove "-Release" at end of liraw release string</p>
<p> M  +1 -1      kdcraw.cpp  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 966164 by mwiesweg:</p>
<p>Add missing functionality from ImageViewUtilities, MetadataManager and<br>
signals.</p>
<p> M  +78 -63    digikamimageview.cpp<br>
 M  +14 -0     digikamimageview.h  </p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 966160 by mwiesweg:</p>
<p>The last missing major piece of functionality for DigikamImageView is<br>
assigning tags/rating including writing metadata for multiple images.<br>
This is done in the event loop thread using kapp-&gt;processEvents() in<br>
AlbumIconView, which is known to be potentially evil.  MetadataManager<br>
provides a simple API for these operations and executes them in threads,<br>
one thread for db operations, one thread for file metadata writing.<br>
Dispatching is done by signals (this is a possible use case for<br>
QtConcurrent as well.  I'm currently not very familiar with that API<br>
though).<br>
Attempt at reporting progress as a summary. Needs testing.</p>
<p> A             metadatamanager.cpp   [License: GPL (v2+)]<br>
 A             metadatamanager.h   [License: GPL (v2+)]<br>
 A             metadatamanager_p.h   [License: GPL (v2+)]</p>
<p>------------------------------------------------------------------------</p>
<p>SVN commit 966228 by cgilles:</p>
<p>polish for MSVC 2008. Sound like it don't like "const class &amp;foo", but<br>
"const class&amp; foo". I have a lots of warnings to clean up...</p>
<p> M  +18 -20    metadatamanager.cpp<br>
 M  +6 -6      metadatamanager.h<br>
 M  +23 -24    metadatamanager_p.h  </p>
<p>------------------------------------------------------------------------<br>
 </p>

<div class="legacy-comments">

</div>