---
date: "2016-08-09T22:29:00Z"
title: "digiKam 5.1.0 is published..."
author: "digiKam"
description: "Dear digiKam fans and users, After a first release 5.0.0 published one month ago, the digiKam team is proud to announce the new release 5.1.0"
category: "news"
aliases: "/node/756"

---

<a href="https://www.flickr.com/photos/digikam/28261264744/in/dateposted-public/"><img src="https://c1.staticflickr.com/9/8806/28261264744_c8dbf42e8d_c.jpg" width="800" height="500"></a>

<p>Dear digiKam fans and users,</p>

<p>
After a <a href="https://www.digikam.org/node/755">first release 5.0.0</a> published one month ago, the digiKam team is proud to announce the new release 5.1.0 of digiKam Software Collection. This version introduces a new huge bugs triage and some fixes following first feedback from end-users.</p>

<p>
This release update internal <a href="http://www.libraw.org">Libraw</a> to last public and pre-release version 0.18.0. This permit to digiKam to support new lead RAW cameras as:

</p><ul>
          <li>Canon 80D, 1300D, 1DX MkII</li>
          <li>Fujifilm X-Pro2, X70, X-E2S</li>
          <li>HTC One A9 and M9</li>
          <li>Leica M and X-U</li>
          <li>Nikon D5</li>
          <li>Olympus Pen F, SH-3</li>
          <li>Panasonic DMC-GX80/85, DMC-TZ80/81/85/ZS60, DMC-TZ100/101/ZS100</li>
          <li>PhaseOne IQ150, IQ180 IR, IQ3</li>
          <li>Samsung Galaxy S7</li>
          <li>Sony ILCA-68, ILCE-6300, RX1R II, RX10 III</li>
</ul>
<p></p>

<p>
In addition, a consolidation of Mysql database interface has been continued. Some optimizations have been introduced to speed-up startup operation, especially to delay the scan from new items after the main interface initialization. Also the option to enable or disable the scan for new items at startup is back in Setup/Miscs dialog page.
</p>

<p>
For furher information, take a look into the list of more than <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=5.1.0&amp;product=digikam&amp;product=kipiplugins">343 files currently closed</a> in Bugzilla.
</p>

<p>
digiKam software collection source code tarball, OSX (&gt;= 10.8) package, and Windows 32/64 bits installers can be downloaded from <a href="http://download.kde.org/stable/digikam/">this repository</a>
</p>

<p>Happy digiKaming!</p>

<div class="legacy-comments">

  <a id="comment-21238"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21238" class="active">Looking for the import</a></h3>    <div class="submitted">Submitted by Rob (not verified) on Wed, 2016-08-10 00:52.</div>
    <div class="content">
     <p>Looking for the import metadata feature from 4.x and can't find it.  Is it gone?  When is updated documentation available (or how can I contribute to updating existing user docs)?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21241"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21241" class="active">For documentation...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2016-08-10 10:49.</div>
    <div class="content">
     ...see instructions from <a href="https://www.digikam.org/contrib#Documentation">here...</a>         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21242"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21242" class="active">import/export metadata...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2016-08-10 11:07.</div>
    <div class="content">
     <p>...must be ported as new BQM tools, but it's not yet done currently.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21244"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21244" class="active">Thanks.  I understand the</a></h3>    <div class="submitted">Submitted by Rob (not verified) on Wed, 2016-08-10 14:40.</div>
    <div class="content">
     <p>Thanks.  I understand the decision has been made to move many things to BQM tools, but that really makes a rough and disjointed workflow for the sake of simplified menus, especially when working with just one or a few photos at a time.</p>
<p>Having said that, I've been using digiKam for years and it's fantastic on the whole.  Thanks to all the developers for the hard work and devotion to a great piece of software.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-21239"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21239" class="active">Digikam never ceases to</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2016-08-10 07:02.</div>
    <div class="content">
     <p>Digikam never ceases to impress! I've been a user for over 10 years and I've never been disappointed. D5.0 was released and I never skipped a beat; can't wait to get packages for 5.1! Thank you!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21243"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21243" class="active">digiKam 5.1.0 crashes</a></h3>    <div class="submitted">Submitted by smaragdus (not verified) on Wed, 2016-08-10 12:12.</div>
    <div class="content">
     <p>On Windows 8 x64 digiKam version 5.1.0 crashes any time I right-click in the Album panel.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21245"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21245" class="active">We need more info...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2016-08-10 17:17.</div>
    <div class="content">
     <p>As a debug trace with debugview program and a GDB backtrace as well if it's not enough.</p>
<p>Look https://www.digikam.org/contrib for details.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21246"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21246" class="active">digiKam-5.1.0-01-Win64: no possibility to change language?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2016-08-10 17:39.</div>
    <div class="content">
     <p>Thx for the great work to all developers and supporters! On Kubuntu it works well, but on the 64-bit-windows-version i miss the option "Switch Application Language" in the help menu.</p>
<p>It would be fine if you could bring this option back in the next update.</p>
<p>Greetings from Germany</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21247"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21247" class="active">Please report this problem to bugzilla...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2016-08-10 18:38.</div>
    <div class="content">
     <p>https://www.digikam.org/support</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21248"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21248" class="active">Same behavior with digiKam-5.1.0-01-Win32</a></h3>    <div class="submitted">Submitted by magiKid (not verified) on Wed, 2016-08-10 19:31.</div>
    <div class="content">
     <p>But I simply copied the content of the folder digikam\data\locale\de from an installation of digiKam-5.0.0 and this seems to work for now.</p>
<p>Also Greetings from Germany;-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21249"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21249" class="active">Problem found...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2016-08-10 23:09.</div>
    <div class="content">
     <p>... and preparing new 5.1.0-02 Windows installers...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21252"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21252" class="active">new 5.1.0-02 Windows installers with i18n...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2016-08-11 08:34.</div>
    <div class="content">
     <p>http://download.kde.org/stable/digikam/digiKam-5.1.0-02-Win32.exe.mirrorlist</p>
<p>http://download.kde.org/stable/digikam/digiKam-5.1.0-02-Win64.exe.mirrorlist</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21253"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21253" class="active">new 5.1.0-02 Windows installer for 64 bit works</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2016-08-11 16:25.</div>
    <div class="content">
     <p>Thx a lot for the very quick response!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21250"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21250" class="active">Bug in 5.00 5.10</a></h3>    <div class="submitted">Submitted by FordPrefect (not verified) on Thu, 2016-08-11 02:57.</div>
    <div class="content">
     <p>Found a bug in both versions.  Currently using 5.10 64 bit windows.<br>
Windows 7 </p>
<p>When starting, the program will generate drive not found 5 times each for every card reader on my computer.  For me, that's 15 times since I have a 3 card reader.  To get past it, I have to press continue over and over again until it stops putting up the messages.</p>
<p>The exact wording is...<br>
"There is no disk in the drive.  Please insert a disk into drive \Device\Harddisk3\DR3."<br>
With the Location changing for each of the card readers.</p>
<p>The thing is... I haven't told it that there are any collection on any of my card readers.  Yet it still is checking for them.  I've tried deleting the config file, but even with a fresh config, it still does it.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21251"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21251" class="active">See this entry in bugzilla</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2016-08-11 07:34.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=366234</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21254"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21254" class="active">Startup time on networked notebook is SO MUCH faster, thanks!</a></h3>    <div class="submitted">Submitted by Heinz (not verified) on Fri, 2016-08-12 18:15.</div>
    <div class="content">
     <p>Startup time on a networked notebook (NFS shares, MySQL DB) is SO MUCH faster (and usable for the first time), thanks! (Used to be approx. 10 minutes...)</p>
<p>Regards,<br>
   Heinz</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21255"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21255" class="active">$date(Y-M-D) ?</a></h3>    <div class="submitted">Submitted by Estelle (not verified) on Sat, 2016-08-13 13:14.</div>
    <div class="content">
     <p>Hello<br>
wonderful soft, this Digikam !!!!<br>
thank you !<br>
I have one question : will it be possible to have a function to add a legende with something like that : $date(Y-M-D HHMMSS) (like the "rename" tool)?<br>
I have searched this and I think it doesn't exist :-(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21256"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21256" class="active">and...</a></h3>    <div class="submitted">Submitted by Estelle (not verified) on Sat, 2016-08-13 13:17.</div>
    <div class="content">
     <p>to add this legend to lot of pictures at the same time ;-)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21257"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21257" class="active">not yet implemented...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2016-08-13 13:17.</div>
    <div class="content">
     <p>...We have a watermark tool in BQM where the kind of feature must be implemented...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21258"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21258" class="active">sorry but I don't understand</a></h3>    <div class="submitted">Submitted by Estelle (not verified) on Sat, 2016-08-13 14:39.</div>
    <div class="content">
     <p>sorry but I don't understand your answer :-(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21259"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21259" class="active">BQM = Batch Queue Manager</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2016-08-13 14:42.</div>
    <div class="content">
     <p>Batch Queue Manager is a tools suppervisor which can process a list of queued operations on a group of files. There is a tool to add text over images in BQM where this kind of feature must be implemented...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21260"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21260" class="active">ah ok I see now :-)
no, in</a></h3>    <div class="submitted">Submitted by Estelle (not verified) on Sat, 2016-08-13 15:42.</div>
    <div class="content">
     <p>ah ok I see now :-)<br>
no, in fact, it's not about writing a text over image but I want to create metadata ("legend" or "title", I don't remember)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-21261"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21261" class="active">Great, it's working again</a></h3>    <div class="submitted">Submitted by etilli (not verified) on Wed, 2016-08-17 21:02.</div>
    <div class="content">
     <p>on Mac OSX 10.11, thank you very much. The 5.0 release was disappointing because it crashed immediately after the starting splash screen came up. But this one works! I am really happy, thank you!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21262"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21262" class="active">Compiling digikam 5.1 under SUSE Tumbleweed fails</a></h3>    <div class="submitted">Submitted by Christoph (not verified) on Tue, 2016-09-06 11:17.</div>
    <div class="content">
     <p>Hejdiho!</p>
<p>I'm compiling and using and loving digikam for many years now. I want to switch to V5.x, but fail to compile it under SUSE Tumbleweed - which is delivered (so far) with digikam 4.14.0 - which means, all support libs and devel files also match V4.x</p>
<p>Errors during cmake seem QT5 related, like</p>
<p>CMake Warning at /usr/lib64/cmake/Qt5/Qt5Config.cmake:26 (find_package):<br>
  Could not find a package configuration file provided by "Qt5Multimedia"<br>
...</p>
<p>or KF5 related:<br>
CMake Warning at /usr/share/ECM/find-modules/FindKF5.cmake:74 (find_package):<br>
  Could not find a package configuration file provided by "KF5AkonadiContact"<br>
...</p>
<p>Any hint or help, what to do, to make compile work on SUSE Tumbleweed (or SUSE Leap 42.1)?</p>
<p>Thanks a lot in advance,</p>
<p>Christoph</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21263"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21263" class="active">compile problem of V5.1 solved.</a></h3>    <div class="submitted">Submitted by Christoph (not verified) on Thu, 2016-09-08 10:25.</div>
    <div class="content">
     <p>To solve this, I needed to install three more packages; unfortunately the names were good matches with the cmake-errors, however, a little twisted:</p>
<p>libq5-qtmultimedia-devel<br>
akonadi-calender-devel<br>
akonadi-contact-devel</p>
<p>Next thing then looks like<br>
[digikam] [Bug 360017] New: build error: opencv_contrib is required...</p>
<p>Tumbleweed comes with OpcenCV V3.x, which cmake seems unable to detect automatically. If one very carefully reads all warnings, incl. the early ones, one finds a hint to use this:</p>
<p>cmake . -DENABLE_OPENCV3=true</p>
<p>So the Bug is not really back, however, cmake could be improved to better tell OpenCV 2.x from 3.x</p>
<p>Anyways, with this, compile works fine now.</p>
<p>Christoph</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21265"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21265" class="active">Compile hint for SUSE Leap 42.1</a></h3>    <div class="submitted">Submitted by Christoph (not verified) on Fri, 2016-09-16 09:11.</div>
    <div class="content">
     <p>Up until now, SUSE Leap 42.1 comes with libkf5kipi V15.12.3-15.1, which is too old to compile digikam V5.1. You'll get an error like this:</p>
<p>.../kpimageslist.cpp:826:39: error: ‘class KIPI::Interface’ has no member named ‘rawFiles’<br>
    QString   rawFilesExt = d-&gt;iface-&gt;rawFiles();</p>
<p>To overcome this, you need at least libkf5kipi V16...</p>
<p>Christoph</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21264"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21264" class="active">apt still on 4.12</a></h3>    <div class="submitted">Submitted by Andrew (not verified) on Sun, 2016-09-11 16:40.</div>
    <div class="content">
     <p>Hi, Any idea when apt will get notice of the current release?  I'm still a linux noob (weaning myself off windows) and couldn't 'make' the current release.  Do I need something from Git to make bootstrap work?<br>
Thanks,<br>
A</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21266"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21266" class="active">answering my own question</a></h3>    <div class="submitted">Submitted by Andrew (not verified) on Sat, 2016-09-17 05:26.</div>
    <div class="content">
     <p>After some duckduckgo'ing  I'll answer my own question for anyone else as inexperienced with this as me: </p>
<p>sudo add-apt-repository ppa:philip5/extra<br>
sudo apt-get update<br>
sudo apt-get install digikam5</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21267"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/756#comment-21267" class="active">import wizard missing?</a></h3>    <div class="submitted">Submitted by edes (not verified) on Tue, 2016-09-20 03:39.</div>
    <div class="content">
     <p>when importing photos from a camera or card, the import wizard is missing in digikam 5.*. is this a bug or a "feature"? i use digikam quite a bit, but not being able to rename and organize photos when downloading is a big downside. i'll stick to 4.14 until this is sorted out.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>