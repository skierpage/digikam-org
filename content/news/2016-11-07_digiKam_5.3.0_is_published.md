---
date: "2016-11-07T23:27:00Z"
title: "digiKam 5.3.0 is published..."
author: "digiKam"
description: "Dear digiKam fans and users, After a 3rd release 5.2.0 published more than one month ago, the digiKam team is proud to announce the new"
category: "news"
aliases: "/node/761"

---

<a href="https://www.flickr.com/photos/digikam/30810582796/in/dateposted-public/" title="DK5.3.0-AppImage-02"><img src="https://c5.staticflickr.com/6/5693/30810582796_f4b3f202e2_c.jpg" width="800" height="225" alt="DK5.3.0-AppImage-02"></a>

<p>Dear digiKam fans and users,</p>

<p>
After a <a href="https://www.digikam.org/node/758">3rd release 5.2.0</a> published more than one month ago, the digiKam team is proud to announce the new release 5.3.0 of digiKam Software Collection. This version introduces an important common solution to deploy the application under Linux using <a href="https://en.wikipedia.org/wiki/AppImage">AppImage bundle</a>.</p>

<p>
AppImage is an open-source project dedicated to provide a simple way to distribute portable software as compressed binary file, that standard user can run as well, without to install special dependencies. All is included into the bundle, as last Qt5 and KF5 frameworks. AppImage use Fuse file-system, which is de-compressed into a temporary directory to start the application. You don't need to install digiKam on your system to be able to use it. Better, you can use the official digiKam from your Linux distribution in parallel, and test the new version without any conflict with one used in production. This permit to quickly test a new release without to wait an official package dedicated for your Linux box. Another AppImage advantage is to be able to provide quickly a pre-release bundle to test last patches applied to source code, outside the releases plan.
</p>

<p>
The bundle include main 3rd-party libraries used by digiKam as Lensfun, OpenCV, and Exiv2. All are optimized for digiKam, and without extra or experimental configurations which can crash the application in special cases. <a href="http://appimage.org/&gt;AppImage SDK&lt;/a&gt; provide a way to integrate the bundle in your desktop to be able to use it quickly later from your preferred graphical interface. The bundle was compiled on an older Linux CentOS 6 to be binary compatible with the most important Linux distribution as Ubuntu, RedHat, OpenSuse, Debian, etc. The most important requirement is that your Linux box must supports Fuse file system in user space.
&lt;/p&gt;

&lt;a href=" https:="" www.flickr.com="" photos="" digikam="" 30847254855="" in="" dateposted-public="" "="" title="DK5.3.0-AppImage-01"><img src="https://c8.staticflickr.com/6/5577/30847254855_3c39ae0f15_c.jpg" width="800" height="450" alt="DK5.3.0-AppImage-01"></a>

</p><p>
The simple way to use the digiKam AppImage is to download the right file for your system (32 bits or 64 bits), to make it executable, and to run it. See <a href="https://github.com/probonopd/AppImageKit/wiki">AppImage documentation</a> if you need more information about.
</p>

<p>
With the continuous help of <a href="https://plus.google.com/u/0/118393660693739377704/about">Wolfgang Scheffner</a>, The digiKam handbook have been updated again and is available on-line at <a href="http://docs.kde.org/development/en/extragear-graphics/digikam/index.html">digiKam</a>. All help are welcome to <a href="https://quickgit.kde.org/?p=digikam-doc.git&amp;a=blobf=TODO">contribute on user manual</a>, by writing sections, proof-reading, translating, etc.
</p>

<p>
For furher information, take a look into the list of more than <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=5.3.0&amp;product=digikam&amp;product=kipiplugins">40 files currently closed</a> in Bugzilla. A special thanks to <a href="https://plus.google.com/u/0/107171232114475191915">Maik Qualmaan</a> who improve code everywhere.
</p>

<p>
digiKam software collection source code tarball, Linux 32/64 bits AppImage bundles, MacOS package, and Windows 32/64 bits installers can be downloaded from <a href="http://download.kde.org/stable/digikam/">this repository</a>
</p>

<p>Happy digiKaming!</p>

<div class="legacy-comments">

  <a id="comment-21282"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21282" class="active">THX!!</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2016-11-08 01:00.</div>
    <div class="content">
     <p>Thx a lot again for your great work!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21322"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21322" class="active">V. 5.3</a></h3>    <div class="submitted">Submitted by Axel (not verified) on Thu, 2016-12-29 18:31.</div>
    <div class="content">
     <p>Dear all,</p>
<p>a small step for digikam, a huge step overall!!</p>
<p>Thank you all, really substantial progresses since several years!! </p>
<p>Absolutely great!!</p>
<p>Axel</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21283"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21283" class="active">HiDPI Support on Linux</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2016-11-08 01:45.</div>
    <div class="content">
     <p>Hi, Anyone has tried this on HiDPI display on Linux. For example, if I have an image with 1000x1000 pixels, when I show it in digikam on a 4K display with scale factor set to 2, the image will display as 2000x2000 when I choose to zoom into 100%. </p>
<p>I don't think this is a right behavior comparing to the other tools on Mac or Windows. The UI elements like label, menu, toolbar should follow the scale factor, but the image display should ignore the scale factor settings. </p>
<p>Anyone can let me know what should I do to make digikam the perfect tool to support this. </p>
<p>best regards</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21288"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21288" class="active">HiDPI</a></h3>    <div class="submitted">Submitted by <a href="http://www.linuxsystems.it/" rel="nofollow">darkbasic</a> (not verified) on Tue, 2016-11-08 12:17.</div>
    <div class="content">
     <p>Hi, I think this is correct because the 2000x2000 image will scale to 1000x1000 once displayed into the monitor, thus being 100%.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21296"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21296" class="active">No, per the result on Windows</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2016-11-11 03:59.</div>
    <div class="content">
     <p>No, per the result on Windows or Mac, the 1000x1000 image do need to show as 1000x1000 no matter what the scale_factor is. </p>
<p>Currently on digikam, it shows on 2000x2000pixels which the image loses its sharpness.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-21284"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21284" class="active">Animated GIF support</a></h3>    <div class="submitted">Submitted by goat (not verified) on Tue, 2016-11-08 05:50.</div>
    <div class="content">
     <p>Any word on when animated gifs will be supported?  It's kind of weird for a photo manager released in 2016 doesn't support one of the most common image formats out there.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21285"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21285" class="active">GIF is supported...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2016-11-08 05:58.</div>
    <div class="content">
     <p>but not gif animated. The reason is simple. Gif is not dedicated to support photograph due to serious color limitations. It just a toy for the internet, that all...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21295"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21295" class="active">digiKam is a cataloger? How</a></h3>    <div class="submitted">Submitted by Simple user (not verified) on Thu, 2016-11-10 15:08.</div>
    <div class="content">
     <p>digiKam is a cataloger? How do I view and manage my images? I do not fully see animated gif in the directory. </p>
<p>"...a toy for the internet..." Hm... Probably digiKam only for professional photographers. Simple users should use simple viewers ... which are able to display the animation in the "toy gif" ...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21298"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21298" class="active">As it's already said,</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2016-11-13 08:37.</div>
    <div class="content">
     <p>As it's already said, animated gif thumb are supported,but not the animation in icon view. Preview must be available through the video player if codec are installed.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21299"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21299" class="active">Seems odd for you to</a></h3>    <div class="submitted">Submitted by goat (not verified) on Sun, 2016-11-13 08:38.</div>
    <div class="content">
     <p>Seems odd for you to completely write off a super common image type.  Why is it that super basic image viewers with no cataloguing abilities can do something with common image types that you won't?  Why completely write off non-professional users?  Why arbitrarily gatekeep people from your software?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21310"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21310" class="active">I waiting for GIF too</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2016-11-19 12:02.</div>
    <div class="content">
     <p>Totally agree with you.<br>
I understand arguments cons, GIF is an old and bad image format so no photograph use it.<br>
But I'm not a photograph, digiKam is a great software and a lot of non-professional users use it everyday, so why not ? Some technical restriction ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21311"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21311" class="active">aGIF thumbnail are still image...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2016-11-19 13:42.</div>
    <div class="content">
     <p>But aGif preview must be show in video player as well</p>
<p>No way to preview AGif animation in icon view. This will overload GUI is you have plenty of file in same album. In fact it's the same deal with all video files and thumbnails.</p>
<p>Under Linux, GStreamer is use in background to show video through Qt5::Multimedia. There is certainly a aGif codec in base|good|ugly|bad GStreamer modules.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21312"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21312" class="active">"No way to preview AGif</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2016-11-21 10:13.</div>
    <div class="content">
     <p>"No way to preview AGif animation in icon view."<br>
Nobody ask for it, we just want Gif view like video view.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21313"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21313" class="active">"Preview must be available</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2016-11-21 10:19.</div>
    <div class="content">
     <p>"Preview must be available through the video player if codec are installed."<br>
Sorry, I didn't see this answer. I'll check my codec, actually it doesn't work for me.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div><a id="comment-21286"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21286" class="active">Thank you for another great</a></h3>    <div class="submitted">Submitted by Ritesh Raj Sarraf (not verified) on Tue, 2016-11-08 10:18.</div>
    <div class="content">
     <p>Thank you for another great release @Digikam Team. I'm sure this is going to be better than the 5.2.0 release. AppImage support is just awesome. This will allow many more users to try it out, and provide invaluable feedback.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21287"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21287" class="active">Group features</a></h3>    <div class="submitted">Submitted by <a href="https://blog.webdnd.de" rel="nofollow">tuxflo</a> (not verified) on Tue, 2016-11-08 11:31.</div>
    <div class="content">
     <p>Thanks a lot for a great new version of this awesome piece of software.</p>
<p>There are still missing some features about the built in Group functions of digikam for example deleting a group (all grouped images) and apply tags to all grouped images. Any ideas when this will be fixed? Nobody else but me using the grouping feature to group RAW and JPG files?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21289"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21289" class="active">Great work</a></h3>    <div class="submitted">Submitted by Emanuele (not verified) on Tue, 2016-11-08 21:43.</div>
    <div class="content">
     <p>Thanks for this new release.<br>
You are great and Digikam is the best photo manager sw out there.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21293"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21293" class="active">There has already been filed a bug for this behaviour</a></h3>    <div class="submitted">Submitted by Patrick van Elk (not verified) on Wed, 2016-11-09 14:33.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=294579</p>
<p>That is indeed one of the minor annoyances of this truly excellent piece of software!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21294"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21294" class="active">5.4.0</a></h3>    <div class="submitted">Submitted by <a href="https://blog.webdnd.de" rel="nofollow">tuxflo</a> (not verified) on Thu, 2016-11-10 14:30.</div>
    <div class="content">
     <p>The Bug says "Fixed in version 5.4.0" so it should not take too long.<br>
Thanks for the info and thanks to the digikam dev-team for fixing it.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-21290"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21290" class="active">Thanks!  Great to see this</a></h3>    <div class="submitted">Submitted by Geoff (not verified) on Wed, 2016-11-09 03:03.</div>
    <div class="content">
     <p>Thanks!  Great to see this wonderful program updated again.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21291"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21291" class="active">"Open With" still missing under MS Windows version</a></h3>    <div class="submitted">Submitted by Ed W (not verified) on Wed, 2016-11-09 04:04.</div>
    <div class="content">
     <p>It seems that there's no option to open an image in an external program in the MS Widows version.  This seems to have disappeared in version 5.0.  Are there plans to add this back?  Without it, the program doesn't fit into my workflow (probably a major difficulty for any workflow) and I'm using version 4.12.  Version 4.12 is still a great program, so I'm not complaining.  I would like to know whether there will be future versions that I can use or whether to stop looking.</p>
<p>Thanks for the software,</p>
<p>Ed W</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21292"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21292" class="active">Due to KIO Support</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2016-11-09 06:50.</div>
    <div class="content">
     <p>This feature do not exist due to non support of KIO outside Linux system.<br>
KIO do not work and is a big puzzle under MacOS and Windows. Native solution need to be implemented instead.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21297"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21297" class="active">How do I talk to the</a></h3>    <div class="submitted">Submitted by zzzone22 (not verified) on Sun, 2016-11-13 04:23.</div>
    <div class="content">
     <p>How do I talk to the community or get support? I've tried the digikam mailing list. No one's responded from digikam users. There is a forum but it's barely ever used. How should I proceed?</p>
<p>You guys ever hope to switch something more modern and centralized. Lack of ease of communication with the community is a barrier to those that might want to use Digikam much less its development.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21300"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21300" class="active">What do you mean by "more</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2016-11-13 08:43.</div>
    <div class="content">
     <p>What do you mean by "more modern and centralized" ?</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21301"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21301" class="active">Where is the AppImage download?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2016-11-13 22:28.</div>
    <div class="content">
     <p>The download section just has repository, tar and git.</p>
<p>Where is the mentioned 32bit/64bit appimage version?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21302"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21302" class="active">in this same repository...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2016-11-13 22:35.</div>
    <div class="content">
     <p>Linux 32 bits: digikam-5.3.0-01-i386.appimage	 06-Nov-2016 15:46	184M<br>
Linux 64 bits: digikam-5.3.0-01-x86-64.appimage  06-Nov-2016 18:13	181M</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21303"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21303" class="active">Here's the link</a></h3>    <div class="submitted">Submitted by <a href="http://brianhoskins.uk" rel="nofollow">Brian</a> (not verified) on Mon, 2016-11-14 20:40.</div>
    <div class="content">
     <p>digikam team, your answer in which you quote the download filenames makes sense if you already know where the repository is.  The op appears to be asking where to find the repository.  Of course, the repository is linked in the post but clearly the op missed it otherwise he/she would not ask the question.</p>
<p>Here's the link: http://download.kde.org/stable/digikam/</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21304"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21304" class="active">I too "missed" the link</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2016-11-15 15:09.</div>
    <div class="content">
     <p>.. and clicked on every link in the post looking for it in vain until I saw this comment.  It appears that the above post html confuses chrome [Version 54.0.2840.59 (64-bit) and causes an invalid link destination from the image.</p>
<p>Thanks for giving me the repo url, being unable to find it was driving me crazy.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21305"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21305" class="active">panorama gone for imageapp?</a></h3>    <div class="submitted">Submitted by Christian Comes (not verified) on Tue, 2016-11-15 19:25.</div>
    <div class="content">
     <p>Hello,</p>
<p>I installed it via imageapp and the panorama funtion does not work, as many of the necessary parts are missing. Is there a way to install them one by one via "find" button? Or is this a fault of the imageApp?</p>
<p>Is it going to be in the repository?</p>
<p>Generally, I love the speed and many bugs are gone, I would love to stick to 5.3, but no panorama is a hard pill to swallow...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21306"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21306" class="active">It's normal...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2016-11-15 22:38.</div>
    <div class="content">
     <p>The missing run-time dependency is Hugin binaries, and all must be installed separately, as a native installation under Linux.<br>
The goal of digiKam AppImage is to bundle digiKam application, not Hugin application. This is the same for OSX and Windows bundles.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21307"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21307" class="active">That is OK, it is just that I</a></h3>    <div class="submitted">Submitted by Christian Comes (not verified) on Wed, 2016-11-16 17:00.</div>
    <div class="content">
     <p>That is OK, it is just that I already have them installed natively (via Digicam 4.16) but they do not work. When I click "find" and go look for them (they are in some system folder), clicking on the file does not change anything- maybe newer hugin is necessary?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21308"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21308" class="active">It is stupid but I will</a></h3>    <div class="submitted">Submitted by Christian Comes (not verified) on Wed, 2016-11-16 17:47.</div>
    <div class="content">
     <p>It is stupid but I will answer myself: I have the newest hugin, so that is not the problem.</p>
<p>enblend and make are available (ver 4.2 and 4.1) but the rest, autooptimizer, cpclean, cpfind, nona, pano_modify and pto2mk are not found.</p>
<p>Any ideas? It is really a show stopper and sad as the program is now great otherwise!<br>
The fix of the issue of the sliding bars in the editor was perfect.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21309"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21309" class="active">Old Panorama Problem with New Hugin</a></h3>    <div class="submitted">Submitted by John Werner (not verified) on Thu, 2016-11-17 04:53.</div>
    <div class="content">
     <p>I've been fighting this problem for a bit.  It has to do with the version of Hugin that is installed.  This article has a good explanation: http://www.zdnet.com/article/how-i-solved-a-digikam-panorama-hiccup/</p>
<p>Now I just have to figure out how I solved it before....</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-21314"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21314" class="active">AppImage and Database?</a></h3>    <div class="submitted">Submitted by Reiner (not verified) on Mon, 2016-11-21 13:12.</div>
    <div class="content">
     <p>Hi,</p>
<p>great to have the option using an AppImage. Please can you give a hint about sharing the data. For the images I guess there is no problem but is it possible to share the database of the old distro digikam version and a newer beta release of digikam running as AppInfo?</p>
<p>Thanks, Reiner</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21315"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21315" class="active">yes...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2016-11-21 13:25.</div>
    <div class="content">
     <p>If older version is 4.x, AppImage will import settings from 4.x and create new 5.x settings in another place. Operation is done while First Run Assistant.</p>
<p>If older version is 5.x, AppImage will use same settings as well.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21316"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21316" class="active">no export to remote computer?</a></h3>    <div class="submitted">Submitted by Christian Comes (not verified) on Tue, 2016-11-29 18:08.</div>
    <div class="content">
     <p>Hello,</p>
<p>I bit the bitter pill and started using hugin as standalone as it is not working inside. But to do that with any kind of acceptable workflow, I need to be able to export selected images to a folder. This export option is not there.</p>
<p>Also it does not let use the export to dropbox (maybe the app is sandboxed?)</p>
<p>All in all, as much as I like the concept of appimage, is there a package for Debian/Ubuntu 16.10?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21317"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21317" class="active">Any way for custom buttons?</a></h3>    <div class="submitted">Submitted by Marco (not verified) on Thu, 2016-12-01 11:29.</div>
    <div class="content">
     <p>Is there any way to make custom buttons and add them to the bars?<br>
For example to memorize a serie of filters, or tags, and filter the images rapidly by only pressing a button?<br>
It would be very useful if you have some photos with a tag "family", or "private", and with a button hide all of this picture, when you have to show your collection to someone not in your family...<br>
I know other people ask for something similar, I think this solution (custom buttons to apply prememorized filters) would be the easiest to implement.<br>
Thanks for this wonderful piece of software!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21320"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21320" class="active">Please ship updated lensfun db in appimage; and some minor bugs</a></h3>    <div class="submitted">Submitted by Buchan (not verified) on Sun, 2016-12-18 10:14.</div>
    <div class="content">
     <p>Thanks for the Appimage, I am using it since the latest stable version of my distro currently doesn't ship new enough versions of KF5 and I would need to go to some effort (uninstall KDE4 or build a VM to compile on) to build updated versions. Although I am mostly happy with Digikam 4.12 provided by the distro, I needed 5.2 or newer for raw support for my new Canon 80D.</p>
<p>However, one of the problems with the appimage is that it is more difficult to make changes, such as a updating the canon dslr file from lensfun, which would normally be trivial.</p>
<p>Unfortunately, while Digikam supports the 80D's raw format, the lensfun database included doesn't (although it was added in the upstream source repo more than a month before the digikam 5.3 release). In future versions of the appimage, please ship the latest version of the lensfun database. Otherwise it's impossible to include the lens auto-correction in a batch job (which is what I did in 4.x to speed up basic common workflow).</p>
<p>(Even better would be to make it easy to get updated lens or body profiles for lensfun in the Digikam settings).</p>
<p>I ran into some other minor issues:<br>
- I wasn't able to download photos from my camera using the appimage of 5.3, whereas 4.12 just works with no fiddling (auto-detects the camera connected by USB), so I currently download on 4.12 Possibly appimage-specific?<br>
- Since my body isn't listed in the lensfun database, I notice that even if I select the body manually and there is an exact match for the lens (e.g. EF-S 17-85) I need to select the lens manually. I think this issue existed in older versions of digikam 4.x but was fixed (maybe in a bug I reported?).<br>
- On 2 different machines the import of the digikam 4 db wasn't successful, it just got stuck and I had to kill the process and choose to not import the db on the next start. Please make sure that anything that can get stuck had a cancel button.</p>
<p>I think those were the only issues compared to 4.x.</p>
<p>Otherwise, 5.3 has been working really well for me, thanks for the efforts of all involved.</p>
<p>Is there any possibility of adding support for connecting to cameras with WiFi support over the network (e.g. 80D, I think 750D/760D and even 1300D now have WiFi)?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21321"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21321" class="active">LensFun 3.2.0</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2016-12-18 11:13.</div>
    <div class="content">
     <p>We use last release of Lensfun 3.2.0 :</p>
<p>http://lensfun.sourceforge.net/</p>
<p>You request want mean to use current implementation of LensFun not yet released. It's enough stable ?</p>
<p>I recommend to ask to LensFun team to release more frequently. Last 3.2.0 is out one year ago...</p>
<p>With AppImage, the hotplug support do not work yet. You must start digiKam, plug camera, and device will be detected.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21324"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21324" class="active">Thanks </a></h3>    <div class="submitted">Submitted by <a href="http://banyannaplespropertymanagement.com" rel="nofollow">Naples Property Management</a> (not verified) on Fri, 2016-12-30 14:41.</div>
    <div class="content">
     <p>Thanks for all the hard work in developing digikam. I use it all the time with my real estate photos for work.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21325"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21325" class="active">Hey, I'm new here. I've got&nbsp;a</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2017-01-03 17:50.</div>
    <div class="content">
     <p>Hey, I'm new here. I've got&nbsp;a question: Currently I've installed digKam 5.1.0 on windows 10 (64-bit) but now I like to do an update on version 5.3.0. Is there any way to save my configuration? Thank you so much!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21326"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/761#comment-21326" class="active">There is nothing special to do...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2017-01-03 20:32.</div>
    <div class="content">
     <p>...before to update. Databases and configurations are saved in separate files. You can backup this file in a removable media is you want.<br>
Updating from 5.1.0 to 5.3.0 will not touch these files while the process.</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>