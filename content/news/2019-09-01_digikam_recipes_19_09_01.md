---
date: "2019-09-01T00:00:00"
title: "digiKam Recipes 19.09.01"
author: "Dmitri Popov"
description: "A new revision of the digiKam Recipes book is available"
category: "news"
---

After a somewhat prolonged quiet period, a new revision of the _digiKam Recipes_ book is ready. This version brings a refreshed book cover as well as new content:

 - Use Git to maintain multiple digiKam profiles and easily switch between them
 - Back up all digiKam settings, so you don't have to configure the application from scratch when you reinstall it
 - Get the most out of the tagging functionality by customizing individual tags
 
All _digiKam Recipes_ readers will receive the updated version of the book automatically and free of charge. The _digiKam Recipes_ book is available from [Google Play Store](https://play.google.com/store/books/details/Dmitri_Popov_digiKam_Recipes?id=T83DBAAAQBAJ) and [Gumroad](https://gumroad.com/l/digikamrecipes/).
