---
date: "2017-03-21"
title: "Contact"
author: "digiKam Team"
description: "Various contact methods for digiKam"
category: "support"
aliases: "/contact"
---

<h3 class="margintop">IRC CHANNEL</h3>

<p>You can find us on the <b>irc.freenode.net</b> network, channels <a href="irc://irc.freenode.net/#digikam" class="external" title="irc://irc.freenode.net/#digikam" rel="nofollow">#digikam</a>
<br>
You can use <a href="http://webchat.freenode.net/?channels=digikam">this url</a> to be connected on #digikam IRC channel using a web interface.
</p>

<h3 class="margintop">TEAM</h3>

<p>See the list of current <a href="https://cgit.kde.org/digikam.git/tree/AUTHORS">digiKam team members</a>.</p>
</div>
