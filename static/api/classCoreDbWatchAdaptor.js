var classCoreDbWatchAdaptor =
[
    [ "CoreDbWatchAdaptor", "classCoreDbWatchAdaptor.html#a73b45a97d84855442420431ce6c615a4", null ],
    [ "albumChange", "classCoreDbWatchAdaptor.html#a0aa66e319e3420ed3856fe8a95ea5edf", null ],
    [ "albumRootChange", "classCoreDbWatchAdaptor.html#a500cc8ebc52dd57370e5ccb293d28be6", null ],
    [ "collectionImageChange", "classCoreDbWatchAdaptor.html#a3251a5f3186dff2da367acc7da4e4c0c", null ],
    [ "imageChange", "classCoreDbWatchAdaptor.html#a8c9909d8fa9e7537b8c8dfd0a08cda08", null ],
    [ "imageTagChange", "classCoreDbWatchAdaptor.html#a4b4dfa8096490ea15a77a6b3c658530b", null ],
    [ "searchChange", "classCoreDbWatchAdaptor.html#aec8b56a3f6c69e45c434477731397506", null ],
    [ "tagChange", "classCoreDbWatchAdaptor.html#a4fa65abf980cdf210920ef3a0b693f8f", null ]
];