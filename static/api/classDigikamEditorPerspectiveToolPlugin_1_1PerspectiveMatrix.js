var classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix =
[
    [ "PerspectiveMatrix", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix.html#a3a99553a3b43b344d9c2f01e4f0f06d2", null ],
    [ "determinant", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix.html#a596f812e14e7e2fb79997e5bc7494dd0", null ],
    [ "invert", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix.html#a862ad7b35019498010341ee7b8ddcbc7", null ],
    [ "multiply", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix.html#abeec9a348529a62a04776418cce2fe5b", null ],
    [ "scale", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix.html#ac667d5ea982236fe845199b4c8172f73", null ],
    [ "transformPoint", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix.html#aec0ec9e9114c2ce90291ad81a98a5480", null ],
    [ "translate", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix.html#a2fe6d7f2a35efaa6217645b5b9655c60", null ],
    [ "coeff", "classDigikamEditorPerspectiveToolPlugin_1_1PerspectiveMatrix.html#a3613ede532f5656e51680c5cf52c2dd9", null ]
];