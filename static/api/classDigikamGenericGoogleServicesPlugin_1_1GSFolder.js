var classDigikamGenericGoogleServicesPlugin_1_1GSFolder =
[
    [ "GSFolder", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html#a40fde98a656313dd06b38ac3aa0dc632", null ],
    [ "canComment", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html#aa5160086934c696d7822a8ebd276c38d", null ],
    [ "description", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html#add746a263014ef1b1ae7bb9dc9a376d5", null ],
    [ "id", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html#a959f162828c5392422df85617afb2151", null ],
    [ "isWriteable", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html#aeba89d6732019fcfb2a7a737c0b95750", null ],
    [ "location", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html#ab842089750a9573bb77a3661572f68b3", null ],
    [ "tags", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html#a77fc42d94d5e524ebfecfde7c8fdffff", null ],
    [ "timestamp", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html#ab3582115ec995f76f49913215ac4f1af", null ],
    [ "title", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html#a4c2ce9749d5132bfdb798460c7837251", null ],
    [ "url", "classDigikamGenericGoogleServicesPlugin_1_1GSFolder.html#a6113ab6b88478450273c4fd7178f99b7", null ]
];