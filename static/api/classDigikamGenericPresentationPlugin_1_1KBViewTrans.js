var classDigikamGenericPresentationPlugin_1_1KBViewTrans =
[
    [ "KBViewTrans", "classDigikamGenericPresentationPlugin_1_1KBViewTrans.html#aa5b4b45582fabfadbc68f4aba69f845a", null ],
    [ "KBViewTrans", "classDigikamGenericPresentationPlugin_1_1KBViewTrans.html#a5ea437e4a8974e3738e93b4e8ebbdcfc", null ],
    [ "~KBViewTrans", "classDigikamGenericPresentationPlugin_1_1KBViewTrans.html#a9ff341a747846fbe21eea4493b739e9f", null ],
    [ "scale", "classDigikamGenericPresentationPlugin_1_1KBViewTrans.html#a439dfc798180000e3dddbe5739d741e6", null ],
    [ "transX", "classDigikamGenericPresentationPlugin_1_1KBViewTrans.html#a6f1c4ea51620c0ebdbcc948cff95e722", null ],
    [ "transY", "classDigikamGenericPresentationPlugin_1_1KBViewTrans.html#a1f50e1db826c58f74cade9b8d12a29be", null ],
    [ "xScaleCorrect", "classDigikamGenericPresentationPlugin_1_1KBViewTrans.html#a06cefddfe09d88e6bfc7ee8d6a4905ca", null ],
    [ "yScaleCorrect", "classDigikamGenericPresentationPlugin_1_1KBViewTrans.html#abf5ddc63f0ce2c05aa8cf92d56cd3d58", null ]
];