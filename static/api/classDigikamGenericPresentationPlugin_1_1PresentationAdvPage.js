var classDigikamGenericPresentationPlugin_1_1PresentationAdvPage =
[
    [ "PresentationAdvPage", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage.html#a25ec87398dbe9e6c360eda7b848c014d", null ],
    [ "~PresentationAdvPage", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage.html#a78e57966c552506ef1fee0e2cd9a36dd", null ],
    [ "readSettings", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage.html#aada50e1b8182e5c6c2d811d9511f9604", null ],
    [ "saveSettings", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage.html#a6cef45a0eeba034204079a9fc004d33c", null ],
    [ "useMillisecondsToggled", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage.html#a194d53ea5f0ebf2da48ce0019e960a2f", null ]
];