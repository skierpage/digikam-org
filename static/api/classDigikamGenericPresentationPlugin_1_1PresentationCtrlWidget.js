var classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget =
[
    [ "PresentationCtrlWidget", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#ab55f5893b1f5333156ac07ac6bc7f121", null ],
    [ "~PresentationCtrlWidget", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#a304121518fb81953f7449e05041bcd68", null ],
    [ "canHide", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#a76ca7b152cc5f15b994178e5c9bfa0bc", null ],
    [ "isPaused", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#a71e20ad940ea925b48fe8f71efb2a467", null ],
    [ "keyPressEvent", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#ac46184de380bc0f79d2ed060a667368c", null ],
    [ "setEnabledNext", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#a76916fb67bc988b5b1779f678c7a2d6c", null ],
    [ "setEnabledPlay", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#ae5e5572079c69bd45bb190765b4ead7a", null ],
    [ "setEnabledPrev", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#a7f229bcfe1e5b3d69413dd820e99e43e", null ],
    [ "setPaused", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#a4b85755941ed0663d35a5bd315472a9c", null ],
    [ "signalClose", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#aeb302abf73456e4603cd4afef1395a77", null ],
    [ "signalNext", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#a90a37f4f4b300357fb4150d8db9e3b85", null ],
    [ "signalPause", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#a90470e8b29e54c65dca24cf7e36fe700", null ],
    [ "signalPlay", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#adcd827c61cdb55a1f8e7ae4eeffef013", null ],
    [ "signalPrev", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#a684ec51a00845d58baac2619d182c3b1", null ],
    [ "PresentationGL", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#a8d465eb7420f159cb4f1c02e0b4bc5d4", null ],
    [ "PresentationWidget", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#a8386d9462ba73106f950c16d0a60709b", null ]
];