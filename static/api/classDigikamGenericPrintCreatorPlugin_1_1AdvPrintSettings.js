var classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings =
[
    [ "CaptionType", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a58593d4111a1eed76b46bd3983885fed", [
      [ "NONE", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a58593d4111a1eed76b46bd3983885feda4128a2f01ec27197b07ec67f4a381bca", null ],
      [ "FILENAME", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a58593d4111a1eed76b46bd3983885feda6eec65b8ef6e05c081871468c6c3d871", null ],
      [ "DATETIME", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a58593d4111a1eed76b46bd3983885fedad1631853c152ff5bc578b12baabd2e3b", null ],
      [ "COMMENT", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a58593d4111a1eed76b46bd3983885fedae61b1fb3a5639d7c8812ad37ce165f02", null ],
      [ "CUSTOM", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a58593d4111a1eed76b46bd3983885fedaec913528c57535eb1f3cbd7d573c9e78", null ]
    ] ],
    [ "ImageFormat", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a32b326e8e32b256e86aafc61d32c7183", [
      [ "JPEG", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a32b326e8e32b256e86aafc61d32c7183a2c0d9183576c0ddad14fa4dba24f6d09", null ],
      [ "PNG", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a32b326e8e32b256e86aafc61d32c7183a2e83b23ecb2a73c0fd19c597fd84aeb6", null ],
      [ "TIFF", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a32b326e8e32b256e86aafc61d32c7183ac650fc4d35241bfa221bfd81bb0dce15", null ]
    ] ],
    [ "Output", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a44ce6727a9069a73a0394e525307a561", [
      [ "PDF", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a44ce6727a9069a73a0394e525307a561a3d23b897fad393f44dfbd95f106667c6", null ],
      [ "FILES", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a44ce6727a9069a73a0394e525307a561aac1741ec58af6263170b563f986ce247", null ],
      [ "GIMP", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a44ce6727a9069a73a0394e525307a561a6bfc9792dd1061a4fb8fbcf511d34686", null ]
    ] ],
    [ "Selection", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a3abbc03ac8ecade21b67593becbb0a47", [
      [ "IMAGES", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a3abbc03ac8ecade21b67593becbb0a47a493b9cca0a6d9ff86307a975f27ff87d", null ],
      [ "ALBUMS", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a3abbc03ac8ecade21b67593becbb0a47a8cb72889d4bc148f1711370727fd7744", null ]
    ] ],
    [ "AdvPrintSettings", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a82714c2f2f07ce7c8331389244258e00", null ],
    [ "~AdvPrintSettings", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a072678eceaf11531ce36116484fc0c25", null ],
    [ "format", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#abdb56efb76105c5b3d1da74168b69125", null ],
    [ "getLayout", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#ad0662e1d8947b6d01f4462fc03ce29a3", null ],
    [ "outputName", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a04d21090120cd95661526b07c31bf4b2", null ],
    [ "readSettings", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#aa04864bf879c0052624bb0e0e59efb6e", null ],
    [ "writeSettings", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a4726a2f62e6bf77d0fe0a14a92ecbbf3", null ],
    [ "captionColor", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#ac98f4d8222c895751274a7e197c9255b", null ],
    [ "captionFont", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a669bd70f50d2c0a356ce39cb5d88e7e7", null ],
    [ "captionSize", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#adce5393e20bb74da2e92d92368120bc2", null ],
    [ "captionTxt", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#aff2775b70daaf894562f8e46832980bd", null ],
    [ "captionType", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a41ff6f11c37c740b5337336b5401b186", null ],
    [ "conflictRule", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a52bf37bbda37b55da0433fa14227f94b", null ],
    [ "currentCropPhoto", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a262d21aabbc89bb579374b6a2fb5acdf", null ],
    [ "currentPreviewPage", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a749a56bc1f6f19ee49ae713b117f93ce", null ],
    [ "disableCrop", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a4e06da6e7f3703d316b8304908ff8cee", null ],
    [ "gimpFiles", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a94b04393e4992d17477c84a60f98d153", null ],
    [ "gimpPath", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#af454d9f52b99b57e71fe9b6d97d60938", null ],
    [ "imageFormat", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#aaa95445709097e459a499f20d7408b21", null ],
    [ "inputImages", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#aeb131588052c9649a8af5aa1f0546b19", null ],
    [ "openInFileBrowser", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a6af6f40352bb9e3bef6426e1666de05f", null ],
    [ "outputDir", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#ac7a75bb4e27c889e5ff04de65276bedc", null ],
    [ "outputLayouts", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a3512594ed6a4694aaf41780f574451bc", null ],
    [ "outputPath", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#ab36e02f0b1ca603ef610ff36a55bc75f", null ],
    [ "outputPrinter", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a8d8d1910288e0acdd84c26d4ac74b404", null ],
    [ "pageSize", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a51ceb12834870fd1ad87180b51de971e", null ],
    [ "photos", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a06644d303b8da241017ffffd8d67fd66", null ],
    [ "photosizes", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a53caab7dfd229474b93190e852ec8a4a", null ],
    [ "printerName", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#aa25897d710eeec2179ad8b6c76fa4501", null ],
    [ "savedPhotoSize", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#ac198624a3aabb3439aac209ec9abe210", null ],
    [ "selMode", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#a3cdb4268dde70cfa16221a888df77b24", null ],
    [ "tempPath", "classDigikamGenericPrintCreatorPlugin_1_1AdvPrintSettings.html#aab8ff4f72e05c51257e4794c8ae83c9f", null ]
];