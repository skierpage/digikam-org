var classDigikamGenericTwitterPlugin_1_1TwUser =
[
    [ "TwUser", "classDigikamGenericTwitterPlugin_1_1TwUser.html#af47938abad0471d9868431ed93244a8b", null ],
    [ "clear", "classDigikamGenericTwitterPlugin_1_1TwUser.html#a1476a880e68f9a4dfc5a4636800abe0b", null ],
    [ "id", "classDigikamGenericTwitterPlugin_1_1TwUser.html#a85037fff6f4a3116d68fede3af2cc8c7", null ],
    [ "name", "classDigikamGenericTwitterPlugin_1_1TwUser.html#a08518968e2ac28b40a59fff7a1d2308f", null ],
    [ "profileURL", "classDigikamGenericTwitterPlugin_1_1TwUser.html#a0316c4322d72240cfc55acb4c556b431", null ],
    [ "uploadPerm", "classDigikamGenericTwitterPlugin_1_1TwUser.html#a809f6f44b09923863a01ecf9fb6fd11c", null ]
];