var classDigikam_1_1AlbumChangeset =
[
    [ "Operation", "classDigikam_1_1AlbumChangeset.html#ae4db80823f758f448bd9dfbcabb0bd6d", [
      [ "Unknown", "classDigikam_1_1AlbumChangeset.html#ae4db80823f758f448bd9dfbcabb0bd6da1f1a45bee6402ab581e95ba739db0ea0", null ],
      [ "Added", "classDigikam_1_1AlbumChangeset.html#ae4db80823f758f448bd9dfbcabb0bd6da748fa396f333992b22ef34caff2089c7", null ],
      [ "Deleted", "classDigikam_1_1AlbumChangeset.html#ae4db80823f758f448bd9dfbcabb0bd6da27681a45b3db9c3989971d90a51d75c7", null ],
      [ "Renamed", "classDigikam_1_1AlbumChangeset.html#ae4db80823f758f448bd9dfbcabb0bd6daf63facc211de7101f828b047eb1c39e2", null ],
      [ "PropertiesChanged", "classDigikam_1_1AlbumChangeset.html#ae4db80823f758f448bd9dfbcabb0bd6dab0769dc67ead03aba04d329a2cbb8da8", null ]
    ] ],
    [ "AlbumChangeset", "classDigikam_1_1AlbumChangeset.html#ad4bb502005e47621ab9f731dfc0aa903", null ],
    [ "AlbumChangeset", "classDigikam_1_1AlbumChangeset.html#a7f618456da59727687a88c6ccc797932", null ],
    [ "albumId", "classDigikam_1_1AlbumChangeset.html#a01b276b28494135dd8e059eb7ddadbdc", null ],
    [ "operation", "classDigikam_1_1AlbumChangeset.html#a246356faa64cf731e76275111fc09706", null ]
];