var classDigikam_1_1DPluginDialog =
[
    [ "DPluginDialog", "classDigikam_1_1DPluginDialog.html#a08f8b701f1262acd4f0be5d0db4c8f88", null ],
    [ "~DPluginDialog", "classDigikam_1_1DPluginDialog.html#a0f5ec776e19c89e057ae21032c50441e", null ],
    [ "restoreDialogSize", "classDigikam_1_1DPluginDialog.html#a5da66f5eb64468e9524230138e09c2a2", null ],
    [ "saveDialogSize", "classDigikam_1_1DPluginDialog.html#a5496bc5fd80e57edd35e652dbb8361ae", null ],
    [ "setPlugin", "classDigikam_1_1DPluginDialog.html#a8f9473a19990a8001d98cde6286d1059", null ],
    [ "m_buttons", "classDigikam_1_1DPluginDialog.html#a06170b9f45856493f30cb0edd368a3c0", null ]
];