var classDigikam_1_1DefaultVersionNamingScheme =
[
    [ "baseName", "classDigikam_1_1DefaultVersionNamingScheme.html#af66fea812ee3f429c38b001e18bccf5d", null ],
    [ "directory", "classDigikam_1_1DefaultVersionNamingScheme.html#a7bc5900e88cff99d735e4eca578a8c13", null ],
    [ "incrementedCounter", "classDigikam_1_1DefaultVersionNamingScheme.html#ac157014dc692fdc1e19e2657dde8df22", null ],
    [ "initialCounter", "classDigikam_1_1DefaultVersionNamingScheme.html#a708d34a9bd8ee83a16f203a9c47457af", null ],
    [ "intermediateDirectory", "classDigikam_1_1DefaultVersionNamingScheme.html#a2962ffbc9451cda4c347e946c21168c3", null ],
    [ "intermediateFileName", "classDigikam_1_1DefaultVersionNamingScheme.html#aedbf29e17e45b08af9594c3fe986ab7f", null ],
    [ "versionFileName", "classDigikam_1_1DefaultVersionNamingScheme.html#a43e446a3bcc621304b4e818dedd8f5cb", null ]
];