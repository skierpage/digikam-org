var classDigikam_1_1DuplicatesProgressObserver =
[
    [ "DuplicatesProgressObserver", "classDigikam_1_1DuplicatesProgressObserver.html#a15c50ad47dc39fb4f513023d14ebc29e", null ],
    [ "~DuplicatesProgressObserver", "classDigikam_1_1DuplicatesProgressObserver.html#a6abf6161ce7a259cf3874e80b3529b48", null ],
    [ "isCanceled", "classDigikam_1_1DuplicatesProgressObserver.html#a566b6944f1dfebc93fc6bee54cc0b6e0", null ],
    [ "processedNumber", "classDigikam_1_1DuplicatesProgressObserver.html#a4145fe9d2ad6d245b1d3099706c3ce10", null ],
    [ "totalNumberToScan", "classDigikam_1_1DuplicatesProgressObserver.html#a556fb7435d0ffa0d4701bd007f26c190", null ]
];