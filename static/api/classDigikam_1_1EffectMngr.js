var classDigikam_1_1EffectMngr =
[
    [ "EffectMethod", "classDigikam_1_1EffectMngr.html#ab6adf2287e8b316aab34286cecde2636", null ],
    [ "EffectType", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290", [
      [ "None", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290a76a6886737b72a45e3ea2b8dc8984cda", null ],
      [ "KenBurnsZoomIn", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290ad7caf293a0aef5fe5ff0b5e7a8ccd28b", null ],
      [ "KenBurnsZoomOut", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290a70176dbd85144f42440e093409e69610", null ],
      [ "KenBurnsPanLR", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290a4e6c9f2e20eb0707dccc55b7d45ecdc1", null ],
      [ "KenBurnsPanRL", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290a34479714bb19c20f67bf024e1fe9b8c0", null ],
      [ "KenBurnsPanTB", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290ac7a3bb929c0bc8349e713929fa86d9a7", null ],
      [ "KenBurnsPanBT", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290ab891a7a5146744e81f17f30873e50bd1", null ],
      [ "Random", "classDigikam_1_1EffectMngr.html#a97320985e4a5a7d570c8608115c83290a4eeb9a944d3ed96fd1d0b82670b25c05", null ]
    ] ],
    [ "EffectMngr", "classDigikam_1_1EffectMngr.html#ac565cc0840a0cae837fd778aa872afb7", null ],
    [ "~EffectMngr", "classDigikam_1_1EffectMngr.html#a21ec4ec1cd4a793f680b86249cd1fee4", null ],
    [ "~Private", "classDigikam_1_1EffectMngr.html#afa4858b662df4085d5599ef302c87341", null ],
    [ "currentFrame", "classDigikam_1_1EffectMngr.html#a0456d10c6561f20f37bc80dddc56d341", null ],
    [ "getRandomEffect", "classDigikam_1_1EffectMngr.html#a11167d57abab5b0403fa0e0d514657bb", null ],
    [ "Private", "classDigikam_1_1EffectMngr.html#ab09067ab2bd46e0cc085bc8d0357928f", null ],
    [ "registerEffects", "classDigikam_1_1EffectMngr.html#a06379dcec070d560aaed9243b22db0f3", null ],
    [ "setEffect", "classDigikam_1_1EffectMngr.html#a5510ded06747b70b70ba6963b1462c9f", null ],
    [ "setFrames", "classDigikam_1_1EffectMngr.html#ad20626c38dfe1d7f36153ac66ac3cf24", null ],
    [ "setImage", "classDigikam_1_1EffectMngr.html#ac3d83134c19090947d894778cb3a0395", null ],
    [ "setOutputSize", "classDigikam_1_1EffectMngr.html#a7775b00a3387d4d62318a2c5a43b0e56", null ],
    [ "eff_curEffect", "classDigikam_1_1EffectMngr.html#ac927367f278b9900ade8f7e5b4488170", null ],
    [ "eff_curFrame", "classDigikam_1_1EffectMngr.html#ab7f0357ff6f8b7055d0b48b2efe852f9", null ],
    [ "eff_effectList", "classDigikam_1_1EffectMngr.html#a156a6a37b8b79b3002a0bf87560d1046", null ],
    [ "eff_image", "classDigikam_1_1EffectMngr.html#a61b542b79f53be530376426da57c8585", null ],
    [ "eff_imgFrames", "classDigikam_1_1EffectMngr.html#a2f136b01a1a9543f9648b32033fb4d17", null ],
    [ "eff_isRunning", "classDigikam_1_1EffectMngr.html#a168e729c7e01f71174da12deb202c7cf", null ],
    [ "eff_outSize", "classDigikam_1_1EffectMngr.html#a18b876351f278bd2f83bfb66bca7591b", null ],
    [ "eff_step", "classDigikam_1_1EffectMngr.html#a1020cb70bb236b0fe9adb8da37fbcb41", null ]
];