var classDigikam_1_1Face_1_1FaceRecognizer =
[
    [ "getLabelInfo", "classDigikam_1_1Face_1_1FaceRecognizer.html#a444dc888b8743e0a0469b24e2610c516", null ],
    [ "getLabelsByString", "classDigikam_1_1Face_1_1FaceRecognizer.html#a064bf0da37999dae4c10da8fcf7bdd58", null ],
    [ "getThreshold", "classDigikam_1_1Face_1_1FaceRecognizer.html#a4fdb07bb955f6545b52b9cc9ce96db3b", null ],
    [ "predict", "classDigikam_1_1Face_1_1FaceRecognizer.html#a187e17d66a2fd2455a866f68fbb050be", null ],
    [ "predict", "classDigikam_1_1Face_1_1FaceRecognizer.html#a5184544d4c791c8e5cc8957f8380a36b", null ],
    [ "predict", "classDigikam_1_1Face_1_1FaceRecognizer.html#a8874e7ca617fbf4ec26e06795d2c902c", null ],
    [ "setLabelInfo", "classDigikam_1_1Face_1_1FaceRecognizer.html#afe6dd3b17c47cb3de2a3ade688b31c51", null ],
    [ "train", "classDigikam_1_1Face_1_1FaceRecognizer.html#a76cb2bfe7b9e5095da03bc9b62c3abd8", null ],
    [ "update", "classDigikam_1_1Face_1_1FaceRecognizer.html#a7bae84e463d3a62dc63572d744b2e847", null ]
];