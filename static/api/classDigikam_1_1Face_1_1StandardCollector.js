var classDigikam_1_1Face_1_1StandardCollector =
[
    [ "PredictResult", "classDigikam_1_1Face_1_1StandardCollector_1_1PredictResult.html", "classDigikam_1_1Face_1_1StandardCollector_1_1PredictResult" ],
    [ "StandardCollector", "classDigikam_1_1Face_1_1StandardCollector.html#a4338a4405b708586ed9be0879d5f21c3", null ],
    [ "collect", "classDigikam_1_1Face_1_1StandardCollector.html#a5a3accff9c538e2955872cb369f99faa", null ],
    [ "getMinDist", "classDigikam_1_1Face_1_1StandardCollector.html#a28e2523d170c8b748ee6b800b2d35619", null ],
    [ "getMinLabel", "classDigikam_1_1Face_1_1StandardCollector.html#a4d6a36d17ae15f37082b177488c3b480", null ],
    [ "getResults", "classDigikam_1_1Face_1_1StandardCollector.html#ad0cc4b88dc060e1730b55f4089c055fd", null ],
    [ "getResultsMap", "classDigikam_1_1Face_1_1StandardCollector.html#a03b9a6d1518de64e09d7948d28149458", null ],
    [ "init", "classDigikam_1_1Face_1_1StandardCollector.html#a5578b00d3947f111e682b343601c7720", null ],
    [ "data", "classDigikam_1_1Face_1_1StandardCollector.html#a5959b4c90458febdf9a42a187712863f", null ],
    [ "minRes", "classDigikam_1_1Face_1_1StandardCollector.html#adb8c5f850cb8331de6c61c1b64da7555", null ],
    [ "threshold", "classDigikam_1_1Face_1_1StandardCollector.html#a5d8060a9d37fbdb5dfd60ce6bb6e4632", null ]
];