var classDigikam_1_1FuzzySearchView =
[
    [ "FuzzySearchTab", "classDigikam_1_1FuzzySearchView.html#aa4e6ab0f3afd2f38a087be5ae6676ab1", [
      [ "DUPLICATES", "classDigikam_1_1FuzzySearchView.html#aa4e6ab0f3afd2f38a087be5ae6676ab1a5f64bd1e688a91e0c0e45f83abf96061", null ],
      [ "SIMILARS", "classDigikam_1_1FuzzySearchView.html#aa4e6ab0f3afd2f38a087be5ae6676ab1a5af36f28ada3f2e993376c2741509ea0", null ],
      [ "SKETCH", "classDigikam_1_1FuzzySearchView.html#aa4e6ab0f3afd2f38a087be5ae6676ab1a1b70143c4fb3fd43bfd55f32e0f0fb12", null ]
    ] ],
    [ "StateSavingDepth", "classDigikam_1_1FuzzySearchView.html#abdf8ad03070bc560242e56575909d6b6", [
      [ "INSTANCE", "classDigikam_1_1FuzzySearchView.html#abdf8ad03070bc560242e56575909d6b6a535f22ead553f373020dce094f0ae735", null ],
      [ "DIRECT_CHILDREN", "classDigikam_1_1FuzzySearchView.html#abdf8ad03070bc560242e56575909d6b6a402f3f077dcfc1835196020f1986fb55", null ],
      [ "RECURSIVE", "classDigikam_1_1FuzzySearchView.html#abdf8ad03070bc560242e56575909d6b6a1d31a55830067a26a4c25eabb48f6d4f", null ]
    ] ],
    [ "FuzzySearchView", "classDigikam_1_1FuzzySearchView.html#a100d627c02ad773be36cde96427c77a5", null ],
    [ "~FuzzySearchView", "classDigikam_1_1FuzzySearchView.html#ab3ed318360556bf2270f9c8d73c17939", null ],
    [ "currentAlbum", "classDigikam_1_1FuzzySearchView.html#a20fd7353f510af7a42d940a08f0ff13b", null ],
    [ "doLoadState", "classDigikam_1_1FuzzySearchView.html#a0ba192306d47a78e0854f84b6abedba5", null ],
    [ "doSaveState", "classDigikam_1_1FuzzySearchView.html#a0e9aac20a493b8917a4aee3db5104fb4", null ],
    [ "dragEnterEvent", "classDigikam_1_1FuzzySearchView.html#acd08ae3a655dab084ecc84c99090522b", null ],
    [ "dragMoveEvent", "classDigikam_1_1FuzzySearchView.html#a87efaf54c5c5199b5673866c1b38ee34", null ],
    [ "dropEvent", "classDigikam_1_1FuzzySearchView.html#ae9a2fbd4914debe919f5de12838b18cf", null ],
    [ "entryName", "classDigikam_1_1FuzzySearchView.html#a226ce9b9faa055120ba386b85509bcdf", null ],
    [ "getConfigGroup", "classDigikam_1_1FuzzySearchView.html#a3dce4d65e29a07ea09d2e7b4746f8350", null ],
    [ "getGroupFromObjectName", "classDigikam_1_1FuzzySearchView.html#a7745d454191d87ac11dd4e32eaed3709", null ],
    [ "getStateSavingDepth", "classDigikam_1_1FuzzySearchView.html#a340090725416a1b9030c67d7d6a77d6e", null ],
    [ "loadState", "classDigikam_1_1FuzzySearchView.html#a884a7c5ec2c82c6b61519a6c035577c2", null ],
    [ "newDuplicatesSearch", "classDigikam_1_1FuzzySearchView.html#a96b55b747a74a9d3717e01e7f5ca8544", null ],
    [ "newDuplicatesSearch", "classDigikam_1_1FuzzySearchView.html#a4c52f67e47e2dc0723bf908f2d16c242", null ],
    [ "newDuplicatesSearch", "classDigikam_1_1FuzzySearchView.html#ac82c5cec598081d9e81fe7f970633621", null ],
    [ "Private", "classDigikam_1_1FuzzySearchView.html#a763cbbcb1a4061bc1fef1f1ba603962c", null ],
    [ "recurse", "classDigikam_1_1FuzzySearchView.html#ab21314552185ae56873520450348b6fd", null ],
    [ "recurseOperation", "classDigikam_1_1FuzzySearchView.html#a232bab57fcaf90767a87d48671770057", null ],
    [ "saveState", "classDigikam_1_1FuzzySearchView.html#a0db6b9c2daff1b521bce63f78d8f1652", null ],
    [ "setActive", "classDigikam_1_1FuzzySearchView.html#ad83cf55f19085817ce99afc9485ef237", null ],
    [ "setConfigGroup", "classDigikam_1_1FuzzySearchView.html#ab1e8a8626362301dc3e5f1c99c30bb67", null ],
    [ "setCurrentAlbum", "classDigikam_1_1FuzzySearchView.html#a9f8e1ca4d5c1e1697ed8d6ec7d099c48", null ],
    [ "setEntryPrefix", "classDigikam_1_1FuzzySearchView.html#ac289161ea3144b78c348c283cbb7777d", null ],
    [ "setItemInfo", "classDigikam_1_1FuzzySearchView.html#a11c8f6d8c94b2d4cb2c208e97c28be25", null ],
    [ "setStateSavingDepth", "classDigikam_1_1FuzzySearchView.html#a1023eaabd6d22bcfcd83b0c7f5c47e9b", null ],
    [ "signalNofificationError", "classDigikam_1_1FuzzySearchView.html#a8fd95ac8af6756e448241f1808a2ce6a", null ],
    [ "active", "classDigikam_1_1FuzzySearchView.html#aefb7643b48685983d64d549e379dce49", null ],
    [ "configPenSketchHueEntry", "classDigikam_1_1FuzzySearchView.html#a430c1f95bc381126fc2d858cacd200bf", null ],
    [ "configPenSketchSaturationEntry", "classDigikam_1_1FuzzySearchView.html#a8d5eed70f630ed9ca4c1af18259da714", null ],
    [ "configPenSketchSizeEntry", "classDigikam_1_1FuzzySearchView.html#a91a5a6594419342dcdc7732081eb6ca9", null ],
    [ "configPenSkethValueEntry", "classDigikam_1_1FuzzySearchView.html#a1ce748be2fa27bb21ea08fa29a153623", null ],
    [ "configResultSketchItemsEntry", "classDigikam_1_1FuzzySearchView.html#adb01a02d174f36eedeb61e6c7a47b542", null ],
    [ "configSimilarsMaxThresholdEntry", "classDigikam_1_1FuzzySearchView.html#a5cfb7598ef961ea8521c66df11549962", null ],
    [ "configSimilarsThresholdEntry", "classDigikam_1_1FuzzySearchView.html#aea132dca93e3ff62aeee09c800dab4dc", null ],
    [ "configTabEntry", "classDigikam_1_1FuzzySearchView.html#a6ec5a5b1b66f69907e6ca0f785d9780c", null ],
    [ "depth", "classDigikam_1_1FuzzySearchView.html#aeb6735ef9639ddfb95132a88308d10dd", null ],
    [ "findDuplicatesPanel", "classDigikam_1_1FuzzySearchView.html#a0110d4f43133a60b3c5ac67a24dc5a0c", null ],
    [ "fingerprintsChecked", "classDigikam_1_1FuzzySearchView.html#a485534572562a0a007b4fb2834bad6af", null ],
    [ "folderView", "classDigikam_1_1FuzzySearchView.html#aa4ecb6c8b86f5df13255813bedab5d01", null ],
    [ "fuzzySearchAlbumSelectors", "classDigikam_1_1FuzzySearchView.html#a2e38d5ed4f98d45cf71e471a76886111", null ],
    [ "group", "classDigikam_1_1FuzzySearchView.html#ad98397a68142e158bfbdb41c6d3a6b6a", null ],
    [ "groupSet", "classDigikam_1_1FuzzySearchView.html#ad81a1720eeb4c8b7dad25ce5fb999a30", null ],
    [ "host", "classDigikam_1_1FuzzySearchView.html#a1dbe4762e8e8245b85af670bf6927b08", null ],
    [ "hsSelector", "classDigikam_1_1FuzzySearchView.html#a636ca17a45e41952a21a6a44455a39e1", null ],
    [ "imageInfo", "classDigikam_1_1FuzzySearchView.html#a5de290ae1167aa29b7fab797e276a3a1", null ],
    [ "imageSAlbum", "classDigikam_1_1FuzzySearchView.html#a146a78064f6e2634d147685ec529a510", null ],
    [ "imageUrl", "classDigikam_1_1FuzzySearchView.html#ac455ae6485c76ad3c95ce651490095a7", null ],
    [ "imageWidget", "classDigikam_1_1FuzzySearchView.html#a3774c17028820a9716d6466edaf1b9f9", null ],
    [ "labelFile", "classDigikam_1_1FuzzySearchView.html#a3e85ec68b61d7b036c437899f7425a88", null ],
    [ "labelFolder", "classDigikam_1_1FuzzySearchView.html#af6ed8ea12cf276e1297210bc5ecd4ecf", null ],
    [ "nameEditImage", "classDigikam_1_1FuzzySearchView.html#a2c1b35526bc4c8b3736b728032834eca", null ],
    [ "nameEditSketch", "classDigikam_1_1FuzzySearchView.html#a3a33259d0db5b1c7fa15a2a07aa0e1d6", null ],
    [ "penSize", "classDigikam_1_1FuzzySearchView.html#afaed83cd94b2c992d9dfbf85f25a8aac", null ],
    [ "prefix", "classDigikam_1_1FuzzySearchView.html#a897f3865b72054997e57f02c54f8a737", null ],
    [ "redoBtnSketch", "classDigikam_1_1FuzzySearchView.html#a3a92436eb4722c789ad5468429c00baf", null ],
    [ "resetButton", "classDigikam_1_1FuzzySearchView.html#a70096024887d60dcfef317ac274fde8c", null ],
    [ "resultsSketch", "classDigikam_1_1FuzzySearchView.html#aa42182415ff958df198e15174d07e480", null ],
    [ "saveBtnImage", "classDigikam_1_1FuzzySearchView.html#adbf9e0ff59d4c161b7f9d8bdbec4e946", null ],
    [ "saveBtnSketch", "classDigikam_1_1FuzzySearchView.html#a9d254b3598ecd3d887ceffd4037ce6dc", null ],
    [ "searchFuzzyBar", "classDigikam_1_1FuzzySearchView.html#a2c3e7a7c3ac6efd8e957547501b4e420", null ],
    [ "searchModel", "classDigikam_1_1FuzzySearchView.html#a6730f28f59da2e2c82258ac62779cff6", null ],
    [ "searchModificationHelper", "classDigikam_1_1FuzzySearchView.html#add393f68b5d5533ad2d4363207171db4", null ],
    [ "searchTreeView", "classDigikam_1_1FuzzySearchView.html#a278dae7c27b87e5982772054447b41fa", null ],
    [ "selColor", "classDigikam_1_1FuzzySearchView.html#a18b4afba60db8b23effeec7b652f836f", null ],
    [ "settings", "classDigikam_1_1FuzzySearchView.html#a2c32c4aafec117285c085859fd8ceba6", null ],
    [ "similarityRange", "classDigikam_1_1FuzzySearchView.html#ac90e5408e6678889eaa1e950e4a4cc60", null ],
    [ "sketchSAlbum", "classDigikam_1_1FuzzySearchView.html#a69da7f0d1fa6126a0fec5b1479679f27", null ],
    [ "sketchSearchAlbumSelectors", "classDigikam_1_1FuzzySearchView.html#a13392dd70810892fa5eb12330b066209", null ],
    [ "sketchWidget", "classDigikam_1_1FuzzySearchView.html#adecac2b6d3662bf58b2239f5c810d68a", null ],
    [ "tabWidget", "classDigikam_1_1FuzzySearchView.html#ac9dcca81eca3a465c835e7d64f1dd1b1", null ],
    [ "thumbLoadThread", "classDigikam_1_1FuzzySearchView.html#a7b5f2081678947800f1140dac3af2a01", null ],
    [ "timerImage", "classDigikam_1_1FuzzySearchView.html#a49eea07a094efb534b39743818fc1475", null ],
    [ "timerSketch", "classDigikam_1_1FuzzySearchView.html#ad590fbe85ac937c0d38beafee8046e0d", null ],
    [ "undoBtnSketch", "classDigikam_1_1FuzzySearchView.html#a2cc2fcea706f6e820b3ce9d2949947fc", null ],
    [ "vSelector", "classDigikam_1_1FuzzySearchView.html#a3fbbcb6d1c0f5ebb2c5a1b4b4a447f91", null ]
];