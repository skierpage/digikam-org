var classDigikam_1_1IOFileSettings =
[
    [ "IOFileSettings", "classDigikam_1_1IOFileSettings.html#a049b724bcb9be9f03f4840dcbbea2cbd", null ],
    [ "~IOFileSettings", "classDigikam_1_1IOFileSettings.html#aca550f696f6ece544748e0482701b0ea", null ],
    [ "HEIFCompression", "classDigikam_1_1IOFileSettings.html#a9ebbf2d569a435e61062cfe051138700", null ],
    [ "HEIFLossLess", "classDigikam_1_1IOFileSettings.html#aed2d59b89eb3a4ef1fc0bf749b75b2f2", null ],
    [ "JPEG2000Compression", "classDigikam_1_1IOFileSettings.html#adcb31b2b95b0112627e393a956b24e73", null ],
    [ "JPEG2000LossLess", "classDigikam_1_1IOFileSettings.html#a425d2924e72c2d8e360e0943c985cce3", null ],
    [ "JPEGCompression", "classDigikam_1_1IOFileSettings.html#aa7e24c58678e8df278bfd84cf23b2c78", null ],
    [ "JPEGSubSampling", "classDigikam_1_1IOFileSettings.html#ad734d72f8cb98d780ac0c76f43721bba", null ],
    [ "PGFCompression", "classDigikam_1_1IOFileSettings.html#a3e8c27d2f24baff69404ea630c03eba2", null ],
    [ "PGFLossLess", "classDigikam_1_1IOFileSettings.html#a783ea3ab8aa2b2f31e9fe60a3f538b2a", null ],
    [ "PNGCompression", "classDigikam_1_1IOFileSettings.html#ab967bf746e422f6b22331a41afd547df", null ],
    [ "rawDecodingSettings", "classDigikam_1_1IOFileSettings.html#a9a09de5812cb6ca68562c5ccd9d957a0", null ],
    [ "rawImportToolIid", "classDigikam_1_1IOFileSettings.html#a22c4720a322a803b2296c9944cfe1550", null ],
    [ "TIFFCompression", "classDigikam_1_1IOFileSettings.html#a7039562f414e7b3ebd5fab7d0e88cd48", null ],
    [ "useRAWImport", "classDigikam_1_1IOFileSettings.html#ade586627e321ad75922beb27feac19cf", null ]
];