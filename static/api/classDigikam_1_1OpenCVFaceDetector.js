var classDigikam_1_1OpenCVFaceDetector =
[
    [ "OpenCVFaceDetector", "classDigikam_1_1OpenCVFaceDetector.html#ac7fa339096b0ad25703cbed62f3afd4a", null ],
    [ "~OpenCVFaceDetector", "classDigikam_1_1OpenCVFaceDetector.html#a77e06b784950694aa07c1b130f366c45", null ],
    [ "accuracy", "classDigikam_1_1OpenCVFaceDetector.html#aa98780f68ef20248044cf69c054fb2fb", null ],
    [ "detectFaces", "classDigikam_1_1OpenCVFaceDetector.html#a175e9fd22de292ce33cec09988a73b3b", null ],
    [ "prepareForDetection", "classDigikam_1_1OpenCVFaceDetector.html#a3bcd728c07568f11f299bbc85728e39f", null ],
    [ "prepareForDetection", "classDigikam_1_1OpenCVFaceDetector.html#a5010bd1cef55170bbe7d0d7ffd9e7f7e", null ],
    [ "Private", "classDigikam_1_1OpenCVFaceDetector.html#a545118bc4414dbf0f217c955217fb493", null ],
    [ "setAccuracy", "classDigikam_1_1OpenCVFaceDetector.html#ad8213dd1b7979f4018aaae457a04fa37", null ],
    [ "setSpecificity", "classDigikam_1_1OpenCVFaceDetector.html#a7f20a6cc323bfeb52e7073a07ddaca11", null ],
    [ "specificity", "classDigikam_1_1OpenCVFaceDetector.html#a016c6ab4b579356a613f50eedf46fe98", null ],
    [ "cascades", "classDigikam_1_1OpenCVFaceDetector.html#ada1f695bc55f29aa00e631d80e8845ba", null ],
    [ "maxDistance", "classDigikam_1_1OpenCVFaceDetector.html#af9efd192cd3b8340e4d8cdf53d35cb31", null ],
    [ "minDuplicates", "classDigikam_1_1OpenCVFaceDetector.html#a5be561c0a79deff4d598d09317328255", null ],
    [ "mutex", "classDigikam_1_1OpenCVFaceDetector.html#a49d5b736109b5bf07ee0ab2f9298d048", null ],
    [ "primaryParams", "classDigikam_1_1OpenCVFaceDetector.html#a20b3871177ec9f37fa0f8226cf4ab222", null ],
    [ "sensitivityVsSpecificity", "classDigikam_1_1OpenCVFaceDetector.html#ad25d49cb04f441c4a79f2e44b976ad25", null ],
    [ "speedVsAccuracy", "classDigikam_1_1OpenCVFaceDetector.html#a617ff14887da01df746e85af2699a755", null ],
    [ "verifyingParams", "classDigikam_1_1OpenCVFaceDetector.html#a04278670fd0a58c1c1fb09fffa0a3906", null ]
];