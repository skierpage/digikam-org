var classDigikam_1_1OpenCVMatData =
[
    [ "OpenCVMatData", "classDigikam_1_1OpenCVMatData.html#a7d3fcdea538f92bb9f8ea3c14196d423", null ],
    [ "OpenCVMatData", "classDigikam_1_1OpenCVMatData.html#abe1708fe9515521df79d1d849ead0110", null ],
    [ "clearData", "classDigikam_1_1OpenCVMatData.html#a1eb4776e8ac2d5f512c1bd8cb24514c7", null ],
    [ "setMat", "classDigikam_1_1OpenCVMatData.html#a60635302f682cc9879fb1eccc018c8b9", null ],
    [ "toMat", "classDigikam_1_1OpenCVMatData.html#afd6f66580ef8ffc515814ae319bfb052", null ],
    [ "cols", "classDigikam_1_1OpenCVMatData.html#a570fbc818bc3d2f6437d7f4d6ab34d68", null ],
    [ "data", "classDigikam_1_1OpenCVMatData.html#ad65b7aba2edbca8135e1719a595fbcc5", null ],
    [ "rows", "classDigikam_1_1OpenCVMatData.html#a8a79f6e8965486530e125f75d440a990", null ],
    [ "type", "classDigikam_1_1OpenCVMatData.html#a2df35c48c6066f011a69c3f28bcd2bd1", null ]
];