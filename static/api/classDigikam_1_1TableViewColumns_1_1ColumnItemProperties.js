var classDigikam_1_1TableViewColumns_1_1ColumnItemProperties =
[
    [ "ColumnCompareResult", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a0714c73efa682e4bcc6dac00989cabd7", [
      [ "CmpEqual", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a0714c73efa682e4bcc6dac00989cabd7ab1fd7950c9141af71b6d915d7619da20", null ],
      [ "CmpABiggerB", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a0714c73efa682e4bcc6dac00989cabd7a3b46fa13fd837bb5e8303e15579e9c0a", null ],
      [ "CmpALessB", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a0714c73efa682e4bcc6dac00989cabd7af64727fba2a786f83c4032b9ac4e2ac7", null ]
    ] ],
    [ "ColumnFlag", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4", [
      [ "ColumnNoFlags", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a3a1867e93424ceda7439df444b42b7a8", null ],
      [ "ColumnCustomPainting", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a2496490a69825be1607d673758561fea", null ],
      [ "ColumnCustomSorting", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4ab775074b18540b93dbbe923cc7977b0c", null ],
      [ "ColumnHasConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#acf85d0f13e2a9d163ab4fbeed5c223e4a22debcee2a26f5a6a7fda09fdd1e3c0c", null ]
    ] ],
    [ "SubColumn", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a69c2c0c873ede3993635584c9afea41e", [
      [ "SubColumnWidth", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a69c2c0c873ede3993635584c9afea41ea03917f4dd8ddbacd7e93ca4a6d8795fe", null ],
      [ "SubColumnHeight", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a69c2c0c873ede3993635584c9afea41eae2f4b7cdd013e3256aa52c51643e2b08", null ],
      [ "SubColumnDimensions", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a69c2c0c873ede3993635584c9afea41ea010464b87542029eba09f735668cab52", null ],
      [ "SubColumnPixelCount", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a69c2c0c873ede3993635584c9afea41eaea637999e7153dce7db714c1b57b9ad7", null ],
      [ "SubColumnBitDepth", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a69c2c0c873ede3993635584c9afea41eaa79a66bd0948ff978bc4bcd393463342", null ],
      [ "SubColumnColorMode", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a69c2c0c873ede3993635584c9afea41ea789ac2ce43377029948916dea8f88d8a", null ],
      [ "SubColumnType", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a69c2c0c873ede3993635584c9afea41eaf37b35648aed2a8d64fe52cd887e974d", null ],
      [ "SubColumnCreationDateTime", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a69c2c0c873ede3993635584c9afea41eaaf3ac3e35552cdc54d974a7e3db7eea2", null ],
      [ "SubColumnDigitizationDateTime", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a69c2c0c873ede3993635584c9afea41ea4428bfb6a4641d53ce1909b4c7b70843", null ],
      [ "SubColumnAspectRatio", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a69c2c0c873ede3993635584c9afea41ea6e95d843cb7d795a1e4a9b06d9815a39", null ],
      [ "SubColumnSimilarity", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a69c2c0c873ede3993635584c9afea41eabf74025aeaabfcb44bb81ba23022e669", null ]
    ] ],
    [ "ColumnItemProperties", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#ac3fb619cdf8b6008b8e4f3d2c102a6f9", null ],
    [ "~ColumnItemProperties", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#aa99b4828161399ed6096c6f9f8713d47", null ],
    [ "columnAffectedByChangeset", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#ac275f599c98cde40ce903a5de6aaf6f4", null ],
    [ "compare", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a1691ce2075f30221f986864a3c6f753a", null ],
    [ "data", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a5d15929a759963dace4476f5bae11725", null ],
    [ "getColumnFlags", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a05346f538bdd8de15978b2369a95d994", null ],
    [ "getConfiguration", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#ace668cd97cd6d0b584332b803a0ec665", null ],
    [ "getConfigurationWidget", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a70ac95911d3bf9ad063cdaed7203a7cd", null ],
    [ "getTitle", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#adc0ac6be3b14051d7e831854283cde50", null ],
    [ "paint", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#ae53896a52f61680c4263e571e19eec7f", null ],
    [ "setConfiguration", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#aaae5e73c6b1b9c0a9b79e19c5bfbbad1", null ],
    [ "signalAllDataChanged", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#ab288b91b167fc9f40490e3d41dee38ee", null ],
    [ "signalDataChanged", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#ac52d1ec1e5839d96de9e1b365582fdfc", null ],
    [ "sizeHint", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a3a5f8ab59b570eb69d51b0d01c452190", null ],
    [ "updateThumbnailSize", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#ac4696688718ef915e4fb096ed8a2efe3", null ],
    [ "configuration", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a1e0c6be1da4fa29ddecaf0bc07a87a37", null ],
    [ "s", "classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#a90a53ac037c5230322f608a687680efa", null ]
];