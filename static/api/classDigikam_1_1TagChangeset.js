var classDigikam_1_1TagChangeset =
[
    [ "Operation", "classDigikam_1_1TagChangeset.html#ad9de389bcba1c76a1a80f4367ebbb4ab", [
      [ "Unknown", "classDigikam_1_1TagChangeset.html#ad9de389bcba1c76a1a80f4367ebbb4aba5677bb7b36f7c7b770e077c21aa88243", null ],
      [ "Added", "classDigikam_1_1TagChangeset.html#ad9de389bcba1c76a1a80f4367ebbb4aba13ddd019a6f91c9ec657d5d591fe7eb9", null ],
      [ "Moved", "classDigikam_1_1TagChangeset.html#ad9de389bcba1c76a1a80f4367ebbb4abaa167abea5e25c2637932a3aab8bdf429", null ],
      [ "Deleted", "classDigikam_1_1TagChangeset.html#ad9de389bcba1c76a1a80f4367ebbb4ababdc7bb82a9aad20452da1bd6e22fa8f7", null ],
      [ "Renamed", "classDigikam_1_1TagChangeset.html#ad9de389bcba1c76a1a80f4367ebbb4abafa4c8988a63e8a66ef4641111aa95b4d", null ],
      [ "Reparented", "classDigikam_1_1TagChangeset.html#ad9de389bcba1c76a1a80f4367ebbb4abae21fcb4b2dd221fabb8c5f095cdd4b24", null ],
      [ "IconChanged", "classDigikam_1_1TagChangeset.html#ad9de389bcba1c76a1a80f4367ebbb4aba68bd4e38d8068d498d3a6ecf2729799e", null ],
      [ "PropertiesChanged", "classDigikam_1_1TagChangeset.html#ad9de389bcba1c76a1a80f4367ebbb4aba8d7686a0973f011889adb62dbc19dd5c", null ]
    ] ],
    [ "TagChangeset", "classDigikam_1_1TagChangeset.html#a0315b6f71357eeaeeabdbb00478f7763", null ],
    [ "TagChangeset", "classDigikam_1_1TagChangeset.html#ad0c874b8d78f2afafa314b88e498da14", null ],
    [ "operation", "classDigikam_1_1TagChangeset.html#a8c4c982666039f0f0cf65f2403197055", null ],
    [ "tagId", "classDigikam_1_1TagChangeset.html#a33cb1b4020d33ab4e7e027608e859915", null ]
];