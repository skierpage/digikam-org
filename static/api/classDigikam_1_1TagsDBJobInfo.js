var classDigikam_1_1TagsDBJobInfo =
[
    [ "TagsDBJobInfo", "classDigikam_1_1TagsDBJobInfo.html#a292fd11efc613c762a5fd34c6407bfae", null ],
    [ "isFaceFoldersJob", "classDigikam_1_1TagsDBJobInfo.html#a60f7bc06f6a362a5fc19c3f61d6ee4f5", null ],
    [ "isFoldersJob", "classDigikam_1_1TagsDBJobInfo.html#a1b1b009cdf8f611bc10488acb3d37026", null ],
    [ "isListAvailableImagesOnly", "classDigikam_1_1TagsDBJobInfo.html#a3dc339cb6285b0debc56a6cb4d844835", null ],
    [ "isRecursive", "classDigikam_1_1TagsDBJobInfo.html#a3998e8c5613f83bf74f126fa68ea3e04", null ],
    [ "setFaceFoldersJob", "classDigikam_1_1TagsDBJobInfo.html#a56a2fd7fed62af22fada7422b170d8bf", null ],
    [ "setFoldersJob", "classDigikam_1_1TagsDBJobInfo.html#afd02b67871ce293b7f6420913a0fa391", null ],
    [ "setListAvailableImagesOnly", "classDigikam_1_1TagsDBJobInfo.html#ac844bb2285615c4a3f37902934f72fc4", null ],
    [ "setRecursive", "classDigikam_1_1TagsDBJobInfo.html#a15cd3f8162a63be05d794988f1453dd2", null ],
    [ "setSpecialTag", "classDigikam_1_1TagsDBJobInfo.html#a13e7f5826ae07f73f0450c7af4987d3f", null ],
    [ "setTagsIds", "classDigikam_1_1TagsDBJobInfo.html#af8ef6f38909f5521716016823b93246a", null ],
    [ "specialTag", "classDigikam_1_1TagsDBJobInfo.html#a1c746cec2be8ff09ebacb8e1d6520481", null ],
    [ "tagsIds", "classDigikam_1_1TagsDBJobInfo.html#a5b4ae21f63e5d36d7ccd7fb58e344898", null ]
];