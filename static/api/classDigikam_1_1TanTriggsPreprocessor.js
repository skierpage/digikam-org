var classDigikam_1_1TanTriggsPreprocessor =
[
    [ "TanTriggsPreprocessor", "classDigikam_1_1TanTriggsPreprocessor.html#ae2c8690cb92f3544ce66240cc13f51a7", null ],
    [ "normalize", "classDigikam_1_1TanTriggsPreprocessor.html#aed6f74099e84dd8deb036261a264347a", null ],
    [ "preprocess", "classDigikam_1_1TanTriggsPreprocessor.html#a19b191de57e2b580de30eab4bfed0ef9", null ],
    [ "preprocessRaw", "classDigikam_1_1TanTriggsPreprocessor.html#a895dab9f84f91fbb99ced2592d70e734", null ],
    [ "alpha", "classDigikam_1_1TanTriggsPreprocessor.html#a1134031a4ccb51d08f7292c415d58a64", null ],
    [ "gamma", "classDigikam_1_1TanTriggsPreprocessor.html#af11750f75034742aec4805aed4fa2a37", null ],
    [ "sigma0", "classDigikam_1_1TanTriggsPreprocessor.html#af8e0450d2cc673919e8841b3a888ea00", null ],
    [ "sigma1", "classDigikam_1_1TanTriggsPreprocessor.html#ab61d3b49bf82b338465fa6bf8992a46a", null ],
    [ "tau", "classDigikam_1_1TanTriggsPreprocessor.html#a3c9be3f449bdfc8808ca482d87373402", null ]
];