var classDigikam_1_1UndoActionIrreversible =
[
    [ "UndoActionIrreversible", "classDigikam_1_1UndoActionIrreversible.html#a6ba3039d67f5de5c7c5ede24bcb718cc", null ],
    [ "~UndoActionIrreversible", "classDigikam_1_1UndoActionIrreversible.html#a4ec478e24dfb3513bfaa1fd231df8b22", null ],
    [ "fileOriginData", "classDigikam_1_1UndoActionIrreversible.html#a832a7070cde0b021738d3d59b497324b", null ],
    [ "fileOriginResolvedHistory", "classDigikam_1_1UndoActionIrreversible.html#af3932452421bbf2532063d836e1a412f", null ],
    [ "getMetadata", "classDigikam_1_1UndoActionIrreversible.html#adbfd2809dfe36bd4c09246f02f9ad0e9", null ],
    [ "getTitle", "classDigikam_1_1UndoActionIrreversible.html#a68fcd30401a224212d7d6cbd97be72f0", null ],
    [ "hasFileOriginData", "classDigikam_1_1UndoActionIrreversible.html#ae7699bd585c82d1beb91d85a34b42e17", null ],
    [ "Private", "classDigikam_1_1UndoActionIrreversible.html#a33c26ac3d4732b6eed1eeef22574e813", null ],
    [ "setFileOriginData", "classDigikam_1_1UndoActionIrreversible.html#ab6dac017863266442c9d910f18ad5bab", null ],
    [ "setMetadata", "classDigikam_1_1UndoActionIrreversible.html#a2d9079762b333054ab52818a1b66fca1", null ],
    [ "setTitle", "classDigikam_1_1UndoActionIrreversible.html#a099f5350e231a4196a2198d22833d565", null ],
    [ "container", "classDigikam_1_1UndoActionIrreversible.html#a95b4dd004a76145b204355a40d366057", null ],
    [ "fileOrigin", "classDigikam_1_1UndoActionIrreversible.html#a91b2ca3af4ba51e418ff01b7f7bc4805", null ],
    [ "fileOriginResolvedHistory", "classDigikam_1_1UndoActionIrreversible.html#a13bdbba5133ad0562699b2ad628341c1", null ],
    [ "title", "classDigikam_1_1UndoActionIrreversible.html#a9671e83e621310440e4a2a1aeae2c8fd", null ]
];