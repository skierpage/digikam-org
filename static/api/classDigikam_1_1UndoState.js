var classDigikam_1_1UndoState =
[
    [ "UndoState", "classDigikam_1_1UndoState.html#a04d1b37310aed3e2605d05a074e3fa65", null ],
    [ "~UndoState", "classDigikam_1_1UndoState.html#af82c0c77c779f65d717780203ea31512", null ],
    [ "hasChanges", "classDigikam_1_1UndoState.html#a59b195e681d6606eb4aa5f73a20a0ac0", null ],
    [ "hasRedo", "classDigikam_1_1UndoState.html#a6cb42c28eb08bd4cc18dd242b3a71eb2", null ],
    [ "hasUndo", "classDigikam_1_1UndoState.html#a110b5bb148a680614c1dfb0bce24b7b9", null ],
    [ "hasUndoableChanges", "classDigikam_1_1UndoState.html#a4764daa94d712cddd259a3d7b88c81ae", null ]
];