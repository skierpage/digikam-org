var classDigikam_1_1UniqueModifier =
[
    [ "IconType", "classDigikam_1_1UniqueModifier.html#ac4e4e6c482648f418e690d9ea783d6bf", [
      [ "Action", "classDigikam_1_1UniqueModifier.html#ac4e4e6c482648f418e690d9ea783d6bfa9770c3aa06a1d1603b3458687db8cc68", null ],
      [ "Dialog", "classDigikam_1_1UniqueModifier.html#ac4e4e6c482648f418e690d9ea783d6bfa5ad450d332874c24a62fd9c44de8d9d2", null ]
    ] ],
    [ "UniqueModifier", "classDigikam_1_1UniqueModifier.html#a7d83b624ac447b8ed3eba78db6667048", null ],
    [ "addToken", "classDigikam_1_1UniqueModifier.html#a0f04ae84ed73801df1834797618a1d95", null ],
    [ "description", "classDigikam_1_1UniqueModifier.html#afbaf5484ecbb3d84de892c80b4858c0a", null ],
    [ "icon", "classDigikam_1_1UniqueModifier.html#a464d899d10bf958fab9cfaf5e7a375d5", null ],
    [ "isValid", "classDigikam_1_1UniqueModifier.html#ac530d86ac9bdc95c04c1af3d39137bde", null ],
    [ "parse", "classDigikam_1_1UniqueModifier.html#ae6bff72c159cc219989b8a84724bf90c", null ],
    [ "parseOperation", "classDigikam_1_1UniqueModifier.html#a2b02a32750e23c425dc8377387a6d2ed", null ],
    [ "Private", "classDigikam_1_1UniqueModifier.html#ad1c2eb3455106f5e0849df408b7a54ec", null ],
    [ "regExp", "classDigikam_1_1UniqueModifier.html#afe9b833706cb27fff1093f7cf952f849", null ],
    [ "registerButton", "classDigikam_1_1UniqueModifier.html#ac95510178a03318696fe117067293a30", null ],
    [ "registerMenu", "classDigikam_1_1UniqueModifier.html#a43cf681665f3ae85d0536a56e806ff56", null ],
    [ "reset", "classDigikam_1_1UniqueModifier.html#aeafa5a91aeafadc6eafdc0f340fa03d2", null ],
    [ "setDescription", "classDigikam_1_1UniqueModifier.html#ad7d9fd530edfeb2791e5d53a0205742d", null ],
    [ "setIcon", "classDigikam_1_1UniqueModifier.html#ad5551e615dcd27fe77777c1337ee86dc", null ],
    [ "setRegExp", "classDigikam_1_1UniqueModifier.html#a08bb2383ee33b31201d0ea1ada7c96bc", null ],
    [ "setUseTokenMenu", "classDigikam_1_1UniqueModifier.html#a27c6d3d231f9d52e2e17fe43c2b6654f", null ],
    [ "signalTokenTriggered", "classDigikam_1_1UniqueModifier.html#a1bcba596fbc4ec57ec70b856fc001c92", null ],
    [ "slotTokenTriggered", "classDigikam_1_1UniqueModifier.html#a1a2d182e7e177880ec029070bb47daff", null ],
    [ "tokens", "classDigikam_1_1UniqueModifier.html#a3f407c9873819bbee962caf20261a4cd", null ],
    [ "useTokenMenu", "classDigikam_1_1UniqueModifier.html#ae7ff7e225d0bbfa6745266d1d89eb87c", null ],
    [ "description", "classDigikam_1_1UniqueModifier.html#a39f740662fa195d12f118cc8aead8900", null ],
    [ "iconName", "classDigikam_1_1UniqueModifier.html#a3f76f25a9c380e4e3805c5b1930f819a", null ],
    [ "parsedResults", "classDigikam_1_1UniqueModifier.html#a0cab394d3fa51893febc8ca7d8ae3086", null ],
    [ "regExp", "classDigikam_1_1UniqueModifier.html#abf9d235905496d367df45872548a25ad", null ],
    [ "tokens", "classDigikam_1_1UniqueModifier.html#a278b98ef6bdc79c3547816122d4f5833", null ],
    [ "useTokenMenu", "classDigikam_1_1UniqueModifier.html#ae887296e961ab838ec16fc01b18a0fe1", null ]
];