var classDigikam_1_1VidSlideSettings =
[
    [ "Selection", "classDigikam_1_1VidSlideSettings.html#a9e966f5e07cfb665eac4e48a76d6d936", [
      [ "IMAGES", "classDigikam_1_1VidSlideSettings.html#a9e966f5e07cfb665eac4e48a76d6d936a9551e17c51d21f2ee5e2332863e9920b", null ],
      [ "ALBUMS", "classDigikam_1_1VidSlideSettings.html#a9e966f5e07cfb665eac4e48a76d6d936aa43047af1c9bb5c77d97b8640de67a45", null ]
    ] ],
    [ "VidBitRate", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5", [
      [ "VBR04", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5aa1aec29ba5e94f67b230d0d665e0a96a", null ],
      [ "VBR05", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5acfb98a3a5a38872f1a4f7564d65096e9", null ],
      [ "VBR10", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5a6a0cd39313302c425f94e1164fcc5dc7", null ],
      [ "VBR12", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5a0da73457cf87a03787689f86fe9eaff8", null ],
      [ "VBR15", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5a46b897f5c8c8dc8da3439b34c758e8b7", null ],
      [ "VBR20", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5ac826a983999add06d352c81005505a14", null ],
      [ "VBR25", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5acc129f5cb822856ca6cceaaf09619651", null ],
      [ "VBR30", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5a0191a9bb3b156bb7165e5e4e40f83265", null ],
      [ "VBR40", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5aa733eb7aa5aef254b4fd8bc4d4ccd300", null ],
      [ "VBR45", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5a3a4988802e6ae1542e7152e09a6f9c8f", null ],
      [ "VBR50", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5a214ee63756b4462899af735cf30f17a4", null ],
      [ "VBR60", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5a61d90f91ed654d97dbb48de58f7a5d34", null ],
      [ "VBR80", "classDigikam_1_1VidSlideSettings.html#afc46ee04b1c1d33f4f2cf522e98b05f5a92e3731cbb75500bcccf6c7f8c5a94f0", null ]
    ] ],
    [ "VidCodec", "classDigikam_1_1VidSlideSettings.html#a31ffc35f10ef1913cd597697811a3494", [
      [ "X264", "classDigikam_1_1VidSlideSettings.html#a31ffc35f10ef1913cd597697811a3494a5c72dd1fa42d53feb3187392353aac58", null ],
      [ "MPEG4", "classDigikam_1_1VidSlideSettings.html#a31ffc35f10ef1913cd597697811a3494aa6d4cef6e1431e7756c610db3bab46bb", null ],
      [ "MPEG2", "classDigikam_1_1VidSlideSettings.html#a31ffc35f10ef1913cd597697811a3494aff4ad053b476cb5b18a65a0586e175bf", null ],
      [ "MJPEG", "classDigikam_1_1VidSlideSettings.html#a31ffc35f10ef1913cd597697811a3494a7077987e3d6ac09389bafe9af7f34770", null ],
      [ "FLASH", "classDigikam_1_1VidSlideSettings.html#a31ffc35f10ef1913cd597697811a3494aa748909d7023c5cdb277f7df8263bcb6", null ],
      [ "WEBMVP8", "classDigikam_1_1VidSlideSettings.html#a31ffc35f10ef1913cd597697811a3494aca44d1ccc65c7c18965849d1971acd37", null ],
      [ "THEORA", "classDigikam_1_1VidSlideSettings.html#a31ffc35f10ef1913cd597697811a3494a8e5da9b732693c86863fc6f8b50df7af", null ],
      [ "WMV7", "classDigikam_1_1VidSlideSettings.html#a31ffc35f10ef1913cd597697811a3494aeec367951bb865eea48c1bb9dc6a818b", null ],
      [ "WMV8", "classDigikam_1_1VidSlideSettings.html#a31ffc35f10ef1913cd597697811a3494aafd1b88982fd4a887eef45b49fb36319", null ],
      [ "WMV9", "classDigikam_1_1VidSlideSettings.html#a31ffc35f10ef1913cd597697811a3494acaba3ce079aa686f745a3326cc0e0d08", null ]
    ] ],
    [ "VidFormat", "classDigikam_1_1VidSlideSettings.html#ad07e01c6f306dd45d811b5113e8d4427", [
      [ "AVI", "classDigikam_1_1VidSlideSettings.html#ad07e01c6f306dd45d811b5113e8d4427ad5b735e00d1c3dfaa72f6ccffc6e9ffe", null ],
      [ "MKV", "classDigikam_1_1VidSlideSettings.html#ad07e01c6f306dd45d811b5113e8d4427aff510caa12edde286de2270ede30a717", null ],
      [ "MP4", "classDigikam_1_1VidSlideSettings.html#ad07e01c6f306dd45d811b5113e8d4427a9a299a6097576f28296e8f1dc46286d0", null ],
      [ "MPG", "classDigikam_1_1VidSlideSettings.html#ad07e01c6f306dd45d811b5113e8d4427a52368062c828549a6d9b985a68822d88", null ]
    ] ],
    [ "VidPlayer", "classDigikam_1_1VidSlideSettings.html#acb32a84783eb45af41e644e5d1a09683", [
      [ "NOPLAYER", "classDigikam_1_1VidSlideSettings.html#acb32a84783eb45af41e644e5d1a09683a5c49e81e267790fdf7963483633c743e", null ],
      [ "INTERNAL", "classDigikam_1_1VidSlideSettings.html#acb32a84783eb45af41e644e5d1a09683a531ef07d683cbf097a1ad93b270c0920", null ],
      [ "DESKTOP", "classDigikam_1_1VidSlideSettings.html#acb32a84783eb45af41e644e5d1a09683ab68de065d0b0d58057bc2c1468f4576c", null ]
    ] ],
    [ "VidStd", "classDigikam_1_1VidSlideSettings.html#a8d83ab3476af24e5e99f6f97fdd8a0b0", [
      [ "PAL", "classDigikam_1_1VidSlideSettings.html#a8d83ab3476af24e5e99f6f97fdd8a0b0ae537bdf651da59edcbd3eb160957441b", null ],
      [ "NTSC", "classDigikam_1_1VidSlideSettings.html#a8d83ab3476af24e5e99f6f97fdd8a0b0a77db0d1601c6ac7ee1ee5265c69ead64", null ]
    ] ],
    [ "VidType", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5", [
      [ "QVGA", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5a7a0700c9a21c0e0d539136040a127a00", null ],
      [ "VCD1", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5af8a81b600902f49ce086631005bc7be3", null ],
      [ "VCD2", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5ab63174f61f1f76b178e0903655f7bf4e", null ],
      [ "HVGA", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5aabc99770c219f5f91101e2d692d0fd27", null ],
      [ "SVCD1", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5a9009122c50f6ea58468bb117e8f8a28c", null ],
      [ "SVCD2", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5aca01db070afea464da3ea5b51d1f6783", null ],
      [ "VGA", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5aff5111666b45d8e4a1c09a78bba35f0a", null ],
      [ "DVD1", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5a0bfbc706b4fc98c9fdd96c8cf3290cb9", null ],
      [ "DVD2", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5a2c3c57c0a239945eefdf35867b413c2a", null ],
      [ "WVGA", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5a9fee58a90249615578165acfd9e5c018", null ],
      [ "XVGA", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5a5dd5b71ad353f115844b26fdefd0b42c", null ],
      [ "HDTV", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5a99b97e33b953e3bbf1ae1980cce4e356", null ],
      [ "BLUERAY", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5a474d5c4001aec8b55161fbf52807e233", null ],
      [ "UHD4K", "classDigikam_1_1VidSlideSettings.html#a9654d0ceb17dc091f01f031ddcf8ddd5a773ddd53570a7880be8dbedad9408b9a", null ]
    ] ],
    [ "VidSlideSettings", "classDigikam_1_1VidSlideSettings.html#a3d8b707bb8f21da8b4315d54fdffa3c0", null ],
    [ "~VidSlideSettings", "classDigikam_1_1VidSlideSettings.html#a2d23fd9da308965fa179fc0118c83f5d", null ],
    [ "readSettings", "classDigikam_1_1VidSlideSettings.html#ad710342d56efb9f39ca991a446e07f5f", null ],
    [ "videoBitRate", "classDigikam_1_1VidSlideSettings.html#a21e1d0237049de22c44fd1966c6f65f5", null ],
    [ "videoCodec", "classDigikam_1_1VidSlideSettings.html#a328c2e310db23841f2e3af2ef97245da", null ],
    [ "videoFormat", "classDigikam_1_1VidSlideSettings.html#a6b5a6518d018801dfb49df347b1c9777", null ],
    [ "videoFrameRate", "classDigikam_1_1VidSlideSettings.html#adedd4dfacca048203733250c50d33182", null ],
    [ "videoSize", "classDigikam_1_1VidSlideSettings.html#ac83157a696fc4cef1580bb3a52c23b2c", null ],
    [ "writeSettings", "classDigikam_1_1VidSlideSettings.html#a9e6249409c43add094ccd85e0b3f76e5", null ],
    [ "abitRate", "classDigikam_1_1VidSlideSettings.html#ae3146ad414b88695fe531140822554aa", null ],
    [ "conflictRule", "classDigikam_1_1VidSlideSettings.html#a15402d8beb4a9f52056fb44799759f14", null ],
    [ "imgFrames", "classDigikam_1_1VidSlideSettings.html#a665b3db24a87297c2acdfdccf60a1f80", null ],
    [ "inputAudio", "classDigikam_1_1VidSlideSettings.html#a469b3c994907b3d5e060a8e03e708e7c", null ],
    [ "inputImages", "classDigikam_1_1VidSlideSettings.html#a10efd2dd5a18415e866f25f06ca2d187", null ],
    [ "outputDir", "classDigikam_1_1VidSlideSettings.html#a557ad36ab3b2c2cd1f18f94058ca86d6", null ],
    [ "outputPlayer", "classDigikam_1_1VidSlideSettings.html#a0ef08dc3926db150a3653dfcb88c0d34", null ],
    [ "outputVideo", "classDigikam_1_1VidSlideSettings.html#a0d8a272503c600f98d9e6fcfc800003a", null ],
    [ "selMode", "classDigikam_1_1VidSlideSettings.html#aec59381fc75a0f1a08c7e604d8aae2c8", null ],
    [ "transition", "classDigikam_1_1VidSlideSettings.html#adedb21c4a3549528938e0411a09257f9", null ],
    [ "vbitRate", "classDigikam_1_1VidSlideSettings.html#aea7902a28feba563a012425ac99a58e9", null ],
    [ "vCodec", "classDigikam_1_1VidSlideSettings.html#a9106dfb76be2f1aad7aba19963b949c6", null ],
    [ "vEffect", "classDigikam_1_1VidSlideSettings.html#a7d73478bdb01f126b69078ff11d8b369", null ],
    [ "vFormat", "classDigikam_1_1VidSlideSettings.html#a3a478c6f75c6d2ea46f48e3bd57849f6", null ],
    [ "vStandard", "classDigikam_1_1VidSlideSettings.html#a3a23570a0c5d773e26921939944b4f26", null ],
    [ "vType", "classDigikam_1_1VidSlideSettings.html#a5abd77a340eb5e3e0bb1a5d83b3c3516", null ]
];