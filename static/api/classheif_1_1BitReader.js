var classheif_1_1BitReader =
[
    [ "BitReader", "classheif_1_1BitReader.html#a3e04be84a624eee0487af9aa6b8d9263", null ],
    [ "get_bits", "classheif_1_1BitReader.html#a495d4deae59ce978eea34b268d710d0f", null ],
    [ "get_bits_fast", "classheif_1_1BitReader.html#a1799b4d33b08576f4f51f608f9d429bb", null ],
    [ "get_current_byte_index", "classheif_1_1BitReader.html#af382e80f0a643d9b3de627d070190652", null ],
    [ "get_svlc", "classheif_1_1BitReader.html#a39986c6965cb212be22cd6f12215b008", null ],
    [ "get_uvlc", "classheif_1_1BitReader.html#ae94d5b640088eed2fef3e5cb3e8147ba", null ],
    [ "peek_bits", "classheif_1_1BitReader.html#a01a8f88aebe64be8c5020f6c1ca39875", null ],
    [ "skip_bits", "classheif_1_1BitReader.html#aced38ba5c8a5b2ec9c791ade7fe6153a", null ],
    [ "skip_bits_fast", "classheif_1_1BitReader.html#a10faebbac70635f30a863851ed5aca00", null ],
    [ "skip_to_byte_boundary", "classheif_1_1BitReader.html#a897f09473c3c0aab6ccc2cb0461726f5", null ]
];