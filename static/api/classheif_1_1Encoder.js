var classheif_1_1Encoder =
[
    [ "Encoder", "classheif_1_1Encoder.html#aab8e5a4e34195e968fe0d1607228d17c", null ],
    [ "get_boolean_parameter", "classheif_1_1Encoder.html#a2d12e766438f545192958cec152bdb57", null ],
    [ "get_integer_parameter", "classheif_1_1Encoder.html#aed5ee7592f546b19ff8626bf99b393fd", null ],
    [ "get_parameter", "classheif_1_1Encoder.html#ae0fd90da99cbf6fd909b815c84960e0c", null ],
    [ "get_string_parameter", "classheif_1_1Encoder.html#a7d29c5bdd9fac0c5a556cce998843cf3", null ],
    [ "list_parameters", "classheif_1_1Encoder.html#a12c8006b417888a5cbed3312efdb372c", null ],
    [ "set_boolean_parameter", "classheif_1_1Encoder.html#a7f76da69bf5f277ae042c6212b7566ad", null ],
    [ "set_integer_parameter", "classheif_1_1Encoder.html#a49f985e8d7579ab0c00d2af406fe1490", null ],
    [ "set_lossless", "classheif_1_1Encoder.html#a4fe6ae0c43a8411e210addb431473023", null ],
    [ "set_lossy_quality", "classheif_1_1Encoder.html#a6205a83f09f92ae73646142642f45545", null ],
    [ "set_parameter", "classheif_1_1Encoder.html#a5c6321da889453a4d0558de0c2897a11", null ],
    [ "set_string_parameter", "classheif_1_1Encoder.html#aaeb74333881c85d9db50163ff5789b50", null ],
    [ "Context", "classheif_1_1Encoder.html#ac26c806e60ca4a0547680edb68f6e39b", null ],
    [ "EncoderDescriptor", "classheif_1_1Encoder.html#a31ae411d00fea671f1d51c092ebc003a", null ]
];