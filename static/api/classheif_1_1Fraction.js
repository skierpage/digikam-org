var classheif_1_1Fraction =
[
    [ "Fraction", "classheif_1_1Fraction.html#a51249af1d87e5f4e636cf53a87f910f7", null ],
    [ "Fraction", "classheif_1_1Fraction.html#ae643c477099daebc99e125733df41438", null ],
    [ "is_valid", "classheif_1_1Fraction.html#a48f39a67cf27c9c54b5b6e2970197b12", null ],
    [ "operator+", "classheif_1_1Fraction.html#a4a456b94148a440a53ae4b0e523e5e41", null ],
    [ "operator-", "classheif_1_1Fraction.html#affd51de882595bcb140ff6ed2fe9d231", null ],
    [ "operator-", "classheif_1_1Fraction.html#a87f50682dd35d067ec160c7208d9873f", null ],
    [ "operator/", "classheif_1_1Fraction.html#a567f66d1c9579dc913b1cd8a6cef38bc", null ],
    [ "round", "classheif_1_1Fraction.html#ad8a9fedce0da1d180214c111d94a28e9", null ],
    [ "round_down", "classheif_1_1Fraction.html#a9f55809642566089b373985befefe496", null ],
    [ "round_up", "classheif_1_1Fraction.html#a6f2d7d2e82ce98758eb075eaf5ce1256", null ],
    [ "denominator", "classheif_1_1Fraction.html#a4481f50d4b8e81efd0d504bd81aab7f6", null ],
    [ "numerator", "classheif_1_1Fraction.html#aebbdf7507421885b61d7ce8140c2e4ed", null ]
];