var namespaceDigikamGenericHtmlGalleryPlugin =
[
    [ "ColorThemeParameter", "classDigikamGenericHtmlGalleryPlugin_1_1ColorThemeParameter.html", "classDigikamGenericHtmlGalleryPlugin_1_1ColorThemeParameter" ],
    [ "CWrapper", "classDigikamGenericHtmlGalleryPlugin_1_1CWrapper.html", "classDigikamGenericHtmlGalleryPlugin_1_1CWrapper" ],
    [ "GalleryConfig", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryConfig.html", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryConfig" ],
    [ "GalleryElement", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryElement.html", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryElement" ],
    [ "GalleryElementFunctor", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryElementFunctor.html", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryElementFunctor" ],
    [ "GalleryInfo", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryInfo.html", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryInfo" ],
    [ "GalleryNameHelper", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryNameHelper.html", "classDigikamGenericHtmlGalleryPlugin_1_1GalleryNameHelper" ],
    [ "HtmlGalleryPlugin", "classDigikamGenericHtmlGalleryPlugin_1_1HtmlGalleryPlugin.html", "classDigikamGenericHtmlGalleryPlugin_1_1HtmlGalleryPlugin" ],
    [ "StringThemeParameter", "classDigikamGenericHtmlGalleryPlugin_1_1StringThemeParameter.html", "classDigikamGenericHtmlGalleryPlugin_1_1StringThemeParameter" ],
    [ "ThemeListBoxItem", "classDigikamGenericHtmlGalleryPlugin_1_1ThemeListBoxItem.html", "classDigikamGenericHtmlGalleryPlugin_1_1ThemeListBoxItem" ],
    [ "XMLAttributeList", "classDigikamGenericHtmlGalleryPlugin_1_1XMLAttributeList.html", "classDigikamGenericHtmlGalleryPlugin_1_1XMLAttributeList" ],
    [ "XMLElement", "classDigikamGenericHtmlGalleryPlugin_1_1XMLElement.html", "classDigikamGenericHtmlGalleryPlugin_1_1XMLElement" ],
    [ "XMLWriter", "classDigikamGenericHtmlGalleryPlugin_1_1XMLWriter.html", "classDigikamGenericHtmlGalleryPlugin_1_1XMLWriter" ]
];