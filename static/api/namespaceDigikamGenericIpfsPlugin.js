var namespaceDigikamGenericIpfsPlugin =
[
    [ "IpfsImagesList", "classDigikamGenericIpfsPlugin_1_1IpfsImagesList.html", "classDigikamGenericIpfsPlugin_1_1IpfsImagesList" ],
    [ "IpfsImagesListViewItem", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem.html", "classDigikamGenericIpfsPlugin_1_1IpfsImagesListViewItem" ],
    [ "IpfsPlugin", "classDigikamGenericIpfsPlugin_1_1IpfsPlugin.html", "classDigikamGenericIpfsPlugin_1_1IpfsPlugin" ],
    [ "IpfsTalkerAction", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerAction.html", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerAction" ],
    [ "IpfsTalkerResult", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerResult.html", "structDigikamGenericIpfsPlugin_1_1IpfsTalkerResult" ]
];