var namespaceDigikamGenericPresentationPlugin =
[
    [ "BlendKBEffect", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect.html", "classDigikamGenericPresentationPlugin_1_1BlendKBEffect" ],
    [ "FadeKBEffect", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect.html", "classDigikamGenericPresentationPlugin_1_1FadeKBEffect" ],
    [ "KBEffect", "classDigikamGenericPresentationPlugin_1_1KBEffect.html", "classDigikamGenericPresentationPlugin_1_1KBEffect" ],
    [ "KBImage", "classDigikamGenericPresentationPlugin_1_1KBImage.html", "classDigikamGenericPresentationPlugin_1_1KBImage" ],
    [ "KBViewTrans", "classDigikamGenericPresentationPlugin_1_1KBViewTrans.html", "classDigikamGenericPresentationPlugin_1_1KBViewTrans" ],
    [ "PresentationAdvPage", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage.html", "classDigikamGenericPresentationPlugin_1_1PresentationAdvPage" ],
    [ "PresentationAudioList", "classDigikamGenericPresentationPlugin_1_1PresentationAudioList.html", "classDigikamGenericPresentationPlugin_1_1PresentationAudioList" ],
    [ "PresentationCaptionPage", "classDigikamGenericPresentationPlugin_1_1PresentationCaptionPage.html", "classDigikamGenericPresentationPlugin_1_1PresentationCaptionPage" ],
    [ "PresentationContainer", "classDigikamGenericPresentationPlugin_1_1PresentationContainer.html", "classDigikamGenericPresentationPlugin_1_1PresentationContainer" ],
    [ "PresentationCtrlWidget", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html", "classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget" ],
    [ "PresentationKB", "classDigikamGenericPresentationPlugin_1_1PresentationKB.html", "classDigikamGenericPresentationPlugin_1_1PresentationKB" ],
    [ "PresentationMngr", "classDigikamGenericPresentationPlugin_1_1PresentationMngr.html", "classDigikamGenericPresentationPlugin_1_1PresentationMngr" ],
    [ "PresentationPlugin", "classDigikamGenericPresentationPlugin_1_1PresentationPlugin.html", "classDigikamGenericPresentationPlugin_1_1PresentationPlugin" ],
    [ "SoundtrackPreview", "classDigikamGenericPresentationPlugin_1_1SoundtrackPreview.html", "classDigikamGenericPresentationPlugin_1_1SoundtrackPreview" ]
];