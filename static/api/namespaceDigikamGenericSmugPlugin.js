var namespaceDigikamGenericSmugPlugin =
[
    [ "SmugAlbum", "classDigikamGenericSmugPlugin_1_1SmugAlbum.html", "classDigikamGenericSmugPlugin_1_1SmugAlbum" ],
    [ "SmugAlbumTmpl", "classDigikamGenericSmugPlugin_1_1SmugAlbumTmpl.html", "classDigikamGenericSmugPlugin_1_1SmugAlbumTmpl" ],
    [ "SmugCategory", "classDigikamGenericSmugPlugin_1_1SmugCategory.html", "classDigikamGenericSmugPlugin_1_1SmugCategory" ],
    [ "SmugMPForm", "classDigikamGenericSmugPlugin_1_1SmugMPForm.html", "classDigikamGenericSmugPlugin_1_1SmugMPForm" ],
    [ "SmugPhoto", "classDigikamGenericSmugPlugin_1_1SmugPhoto.html", "classDigikamGenericSmugPlugin_1_1SmugPhoto" ],
    [ "SmugPlugin", "classDigikamGenericSmugPlugin_1_1SmugPlugin.html", "classDigikamGenericSmugPlugin_1_1SmugPlugin" ],
    [ "SmugUser", "classDigikamGenericSmugPlugin_1_1SmugUser.html", "classDigikamGenericSmugPlugin_1_1SmugUser" ],
    [ "SmugWidget", "classDigikamGenericSmugPlugin_1_1SmugWidget.html", "classDigikamGenericSmugPlugin_1_1SmugWidget" ]
];