var namespaceDigikam_1_1Haar =
[
    [ "Calculator", "classDigikam_1_1Haar_1_1Calculator.html", "classDigikam_1_1Haar_1_1Calculator" ],
    [ "ImageData", "classDigikam_1_1Haar_1_1ImageData.html", "classDigikam_1_1Haar_1_1ImageData" ],
    [ "SignatureData", "classDigikam_1_1Haar_1_1SignatureData.html", "classDigikam_1_1Haar_1_1SignatureData" ],
    [ "SignatureMap", "classDigikam_1_1Haar_1_1SignatureMap.html", "classDigikam_1_1Haar_1_1SignatureMap" ],
    [ "WeightBin", "classDigikam_1_1Haar_1_1WeightBin.html", "classDigikam_1_1Haar_1_1WeightBin" ],
    [ "Weights", "classDigikam_1_1Haar_1_1Weights.html", "classDigikam_1_1Haar_1_1Weights" ]
];