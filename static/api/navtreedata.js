/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "digiKam", "index.html", [
    [ "digiKam API reference page.", "index.html", [
      [ "Source Code Directories", "index.html#sourcedirs", null ],
      [ "External Dependencies", "index.html#externaldeps", [
        [ "Dependencies To Checkout All Source Code", "index.html#depscheckout", null ],
        [ "Dependencies To Process Translations Files (optional)", "index.html#depstrans", null ],
        [ "Dependencies To Compile And Link Source Code", "index.html#depscomplink", null ]
      ] ],
      [ "Get Source Code", "index.html#getsourcecode", [
        [ "Software Components", "index.html#softcomponents", null ]
      ] ],
      [ "Development Envirronnement", "index.html#develenv", null ],
      [ "Cmake Configuration Options", "index.html#cmakeoptions", [
        [ "Top Level Configuration", "index.html#topleveloptions", null ],
        [ "Core Configuration", "index.html#coreoptions", null ]
      ] ],
      [ "Setup Local Compilation and Run-Time", "index.html#setuplocaldev", null ],
      [ "Debug Traces At Run-Time", "index.html#debugtraces", [
        [ "Logging Using an Environment Variable", "index.html#Enable", null ],
        [ "Logging Categories in digiKam", "index.html#digiKamloggincat", null ],
        [ "Further Reading", "index.html#moreaboutloggincat", null ]
      ] ],
      [ "Cmake compilation rules", "index.html#cmakecompilrules", [
        [ "Introduction", "index.html#cmakeintro", null ],
        [ "CMake Implementation Details", "index.html#cmakeimpldetails", [
          [ "Include Directories", "index.html#includedirs", null ],
          [ "Shared Libraries", "index.html#sharedlibs", null ],
          [ "Static Libraries", "index.html#staticlibs", null ],
          [ "Object Libraries", "index.html#objlibs", null ]
        ] ]
      ] ],
      [ "Contribute To The Code", "index.html#codecontrib", [
        [ "Starting With Open-Source", "index.html#startopensource", null ],
        [ "Source Code Formatting", "index.html#codeformat", [
          [ "Indentation length", "index.html#indentlength", null ],
          [ "Tabs vs Spaces", "index.html#tabsspaces", null ],
          [ "Line length", "index.html#linelength", null ],
          [ "Bracketing", "index.html#bracketing", null ],
          [ "Positioning of Access modifiers", "index.html#accessmod", null ]
        ] ],
        [ "Class, file and Variable names", "index.html#itemsnames", [
          [ "Class and filenames", "index.html#classfilenames", null ],
          [ "Protected Member variables", "index.html#protectvars", null ],
          [ "Non-Member variables", "index.html#nomembvars", null ],
          [ "Private Member variables", "index.html#privmembvars", null ]
        ] ],
        [ "Comments and Whitespace", "index.html#commentsspaces", null ],
        [ "Header Files", "index.html#headerfiles", null ],
        [ "Automatic source code formatting", "index.html#autocodeformat", null ],
        [ "General recommendations", "index.html#generalrecommend", null ],
        [ "GDB Backtrace", "index.html#gdbbacktrace", null ],
        [ "Memory Leak", "index.html#memleak", null ],
        [ "Profiling With Cachegrind", "index.html#profilingcode", null ],
        [ "Unit Testing / Automated Testing", "index.html#unittests", null ],
        [ "Checking For Corrupt Qt Signal Slot Connection", "index.html#corruptedsignalslot", null ],
        [ "Finding Duplicated Code", "index.html#dupcodes", null ],
        [ "API Documentation Validation, User Documentation Validation, Source Code Checking", "index.html#docvalidcodecheck", null ],
        [ "Usability Issues", "index.html#appusability", null ],
        [ "Generate API Documentation", "index.html#apidoc", null ],
        [ "Speed Up The Code-Compile-Test Cycle", "index.html#speedupcompil", null ],
        [ "Working With Branches From Git Repository", "index.html#gitbranches", null ]
      ] ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", "namespacemembers_dup" ],
        [ "Functions", "namespacemembers_func.html", "namespacemembers_func" ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", "namespacemembers_eval" ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", "functions_enum" ],
        [ "Enumerator", "functions_eval.html", "functions_eval" ],
        [ "Properties", "functions_prop.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Examples", "examples.html", "examples" ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"classAlgo__TB__IntraPredMode__BruteForce.html#ace022ffaf8d88aba411ee1b869fe6083",
"classCodingOption.html#a228353f47facd3f1be2f57b13d913cdf",
"classDigikamBqmAssignTemplatePlugin_1_1AssignTemplate.html#afa76b46ac346747b289ce17be3124a72a3e0af80bcff0ed3b2a81c1994ebf2d50",
"classDigikamBqmBCGCorrectionPlugin_1_1BCGCorrection.html#ae76700064b9ffacb124a8163dc693546",
"classDigikamBqmBlurPlugin_1_1Blur.html#abfbf72c25a9b65b9fb4b572d46fca3ef",
"classDigikamBqmChannelMixerPlugin_1_1ChannelMixer.html#aaad2062397c2e203ab15832241c12b7a",
"classDigikamBqmColorFXPlugin_1_1ColorFX.html#a847551f8c091a7b598e5782348bafbd8",
"classDigikamBqmConvert8To16Plugin_1_1Convert8To16Plugin.html#aebb937c014590747d26b38a5ddc4f420",
"classDigikamBqmConvertToHeifPlugin_1_1ConvertToHEIF.html#a2fb5df5353f97d75bf26b3189cbdb250",
"classDigikamBqmConvertToJpegPlugin_1_1ConvertToJPEG.html#a13e4394c3219e9046ced4da7f43a3337",
"classDigikamBqmConvertToPgfPlugin_1_1ConvertToPgfPlugin.html#ac925a83b285eaae76f520aeb77773901",
"classDigikamBqmConvertToTiffPlugin_1_1ConvertToTiffPlugin.html#a5ab9e1bce54d762f0e65acc11069896b",
"classDigikamBqmFilmGrainPlugin_1_1FilmGrain.html#ac596446d1ca3d457b7a8a6c9f360d763",
"classDigikamBqmHSLCorrectionPlugin_1_1HSLCorrection.html#aaed484c6d693e73ce09d405803355d04",
"classDigikamBqmInvertPlugin_1_1Invert.html#a8f2e313efd27dc6300f64bfe6c92cdc5",
"classDigikamBqmNoiseReductionPlugin_1_1NoiseReduction.html",
"classDigikamBqmRemoveMetadataPlugin_1_1RemoveMetadataPlugin.html#a79277bceb0e40c90ed5cc20b17e68e7c",
"classDigikamBqmSharpenPlugin_1_1Sharpen.html#a8d1951cc07e2e31fd6763a8f2abfd4c7",
"classDigikamBqmUserScriptPlugin_1_1UserScriptPlugin.html#afde82d7d92a1d75dbae094a66c669057",
"classDigikamEditorAntivignettingToolPlugin_1_1AntiVignettingToolPlugin.html#a8140de1f7aa50c20fd137921f0ad63a8",
"classDigikamEditorChannelMixerToolPlugin_1_1ChannelMixerToolPlugin.html#a57e58bf895ef8df3cb50694aee9ba518",
"classDigikamEditorEmbossToolPlugin_1_1EmbossToolPlugin.html#a35d45cfa520c713de9348130b0a4d26c",
"classDigikamEditorHotPixelsToolPlugin_1_1HotPixelFixer.html#a78acc4ba12fe4642589ec49b57abebfb",
"classDigikamEditorLocalContrastToolPlugin_1_1LocalContrastToolPlugin.html#a5ab9e1bce54d762f0e65acc11069896b",
"classDigikamEditorRatioCropToolPlugin_1_1RatioCropToolPlugin.html#a586144ad9625ffa1f503ad74e341c639",
"classDigikamGenericBoxPlugin_1_1BOXNewAlbumDlg.html#a335987a85da0b9b0bd083f3b85250370",
"classDigikamGenericCalendarPlugin_1_1CalendarPlugin.html#a996af3ec25ecb4c44788435b8ca5d079",
"classDigikamGenericDropBoxPlugin_1_1DBWidget.html#ae542c0fce03605dccf2e61510a86862d",
"classDigikamGenericFaceBookPlugin_1_1FbNewAlbumDlg.html#aa291ce3b1dd7e48df59ad39a3a88393f",
"classDigikamGenericFlickrPlugin_1_1FlickrNewAlbumDlg.html#a4c5149c75a3458e8018b2cb7a9ea8c5b",
"classDigikamGenericGeolocationEditPlugin_1_1GeoDataParser.html",
"classDigikamGenericGoogleServicesPlugin_1_1GSWidget.html#ae542c0fce03605dccf2e61510a86862d",
"classDigikamGenericHtmlGalleryPlugin_1_1XMLElement.html#ac343d8db6de4a1b743d8ea8331e41870",
"classDigikamGenericImgUrPlugin_1_1ImgurImagesList.html#a25e9eb94fac0215c53edc49ce93fec15",
"classDigikamGenericJAlbumPlugin_1_1JAlbumPlugin.html#a4744e611898b250b3b17f4b34830cba0",
"classDigikamGenericMetadataEditPlugin_1_1IPTCSubjects.html#aa9ba18d558c38028884266b4ae7eff7c",
"classDigikamGenericPanoramaPlugin_1_1AutoCropTask.html#a76d4c758d68120c53b8f97de2dceb215",
"classDigikamGenericPanoramaPlugin_1_1CpFindTask.html#a74500f63bc3a535bb209b6e4bc490d78",
"classDigikamGenericPanoramaPlugin_1_1NonaBinary.html#a6001d6c58a828e9c8056182b0284fec4",
"classDigikamGenericPinterestPlugin_1_1PPlugin.html#a80bd292cc2e8e522065ba9f4e6811d37",
"classDigikamGenericPresentationPlugin_1_1PresentationCtrlWidget.html#a7f229bcfe1e5b3d69413dd820e99e43e",
"classDigikamGenericPrintCreatorPlugin_1_1AtkinsPageLayoutTree.html#ad1548c9af1479834ae73ff6702164a50",
"classDigikamGenericRajcePlugin_1_1RajceWindow.html#a6f9919249e2e8aa237fabb2ed72e75a2",
"classDigikamGenericSendByMailPlugin_1_1KmailBinary.html#af51d5b421ea8568f89ae3f73e6d32aab",
"classDigikamGenericSlideShowPlugin_1_1SlideShowPlugin.html#a5ab9e1bce54d762f0e65acc11069896b",
"classDigikamGenericTimeAdjustPlugin_1_1TimeAdjustList.html#afe7491c68d23c7887031b81e2e763915",
"classDigikamGenericUnifiedPlugin_1_1WSTalker.html#a2d3cfdb303894d19f2435cc7bdac1363",
"classDigikamGenericYFPlugin_1_1YandexFotkiAlbum.html#acca62ca409eea18b6dfef3a02152298d",
"classDigikamJPEGDImgPlugin_1_1DImgJPEGPlugin.html#a734431c4d1fa8def07ebbdd489172e1b",
"classDigikamRAWDImgPlugin_1_1DImgRAWLoader.html#ab0df193685b982cf8ef498711e748ab0",
"classDigikam_1_1AbstractAlbumTreeView.html#a340090725416a1b9030c67d7d6a77d6e",
"classDigikam_1_1AbstractSearchGroupContainer.html#a1ed2dea30b95d2c8430e901488239ccc",
"classDigikam_1_1ActionSortFilterProxyModel.html#a625802b662e24b0b3fcb5f3f8648ae85a5f2ca32c1bbed81c8055538a76944c8b",
"classDigikam_1_1AlbumManager.html#abaa87810eee3e8cb3ef59a81f4299a0f",
"classDigikam_1_1AlbumTreeView.html#a10d31f6cc26348ac98bd5851c5ed2f3d",
"classDigikam_1_1AntiVignettingFilter.html#a26e883ed7e4a720811fa9486b5c8ebff",
"classDigikam_1_1ApplicationSettings.html#a80a847ddefe3c7d5eea1aec56b642059",
"classDigikam_1_1AssignNameWidgetStates.html#a0848a631b843d9b1481b3a928896d9b0",
"classDigikam_1_1AutoLevelsFilter.html#aeefd023bbde2c65c61431ff2c08bd8a9",
"classDigikam_1_1BdEngineBackendPrivate_1_1BusyWaiter.html#abf6ab5b9f8b39ad7ae025316cfd79a3d",
"classDigikam_1_1CategorizedItemModel.html#a7ad3851898a606ee574cf5a35a63437e",
"classDigikam_1_1CollectionScanner.html#a3357d74ebf3bdc46ca56a908ba4bac29",
"classDigikam_1_1CommentInfo.html",
"classDigikam_1_1CoreDbPrivilegesChecker.html#ae305eee975e3ac80a675600b32406d15",
"classDigikam_1_1DBJobInfo.html#a3dc339cb6285b0debc56a6cb4d844835",
"classDigikam_1_1DColor.html#acdea1d16a339a0558526ef9903bae20c",
"classDigikam_1_1DConfigDlgWdg.html#a716e69afe226ef42ab947f8ee6bd0670",
"classDigikam_1_1DExpanderBoxExclusive.html#a5b9450ec23a70881e212fd4c6448eaee",
"classDigikam_1_1DImgBuiltinFilter.html#a7a831412f72c111ae250859e1a9bb940",
"classDigikam_1_1DImgThreadedAnalyser.html#a9cdd61f25e27f10dd79612e3f8563d47",
"classDigikam_1_1DItemsListView.html#ad08edfcee9ac5134a51571bae90539e7a1a284f6bb4743b05f63061c117618592",
"classDigikam_1_1DMetadata.html#a96d7dd9eafd14ab5e5cd2b17ec38c977",
"classDigikam_1_1DNotificationWidget.html#a2eff5d1ce6a9c983d544112f213ad846",
"classDigikam_1_1DPluginRawImport.html#a586144ad9625ffa1f503ad74e341c639",
"classDigikam_1_1DTextLabelValue.html#a808d91a0b119dc0ea7963c5d9008a22e",
"classDigikam_1_1DatabaseSettingsWidget.html#ac2f88576ab356e1f8c2d0f1652734013",
"classDigikam_1_1DateOption.html#afe9b833706cb27fff1093f7cf952f849",
"classDigikam_1_1DbEngineParameters.html#a5d25e723bc649612a2dcea4c8066e8bf",
"classDigikam_1_1DetectionWorker.html#a840cab83db58ee78572d90a3fd546242",
"classDigikam_1_1DigikamItemDelegate.html#a9df805d548fe487c74dee1272dddb1bd",
"classDigikam_1_1DigikamItemView.html#ac85a9a587cd94645c82ea46e89ebfe25",
"classDigikam_1_1EditorCore.html#af16650743bf90a0885949eaf786bad9b",
"classDigikam_1_1EditorWindow.html#afdad0db3cfd95d7747f54a46108bce9fabfbc46c12169f73993df4264281d997d",
"classDigikam_1_1EqualizeFilter.html#adfb7df7c72047122f4806e498001c256",
"classDigikam_1_1FaceGroup.html#aaaf332f3ee71fd74f522e716387e0a6d",
"classDigikam_1_1FacePipelineFaceTagsIface.html#a57c1ea37382dcd3728c00adb43133da3",
"classDigikam_1_1FaceRejectionOverlayButton.html#aa660bae5c407b0964e86db49ddc079ed",
"classDigikam_1_1FileActionMngr.html#a292f6a630f00f5e4aadd29f2a95fcb64",
"classDigikam_1_1FilePropertiesOption.html#a5460d3b2941be812388ae35652946e46",
"classDigikam_1_1FilmProfile.html#a880ecff3c41956ba0d22122905707aa9",
"classDigikam_1_1GPSDBJobsThread.html#a54fe5d3899121d7df8fe8d72c40cb1b0",
"classDigikam_1_1GeoIfaceSharedData.html#afc01fffb60fb1c3748b5b6d3a566e1e4",
"classDigikam_1_1Graph_1_1Vertex.html#aa81527acd60e54fe53d82253f42666e7",
"classDigikam_1_1HidingStateChanger.html#a787db58735cc93800f826dd1686e4f2a",
"classDigikam_1_1IccPostLoadingManager.html#a77242fb2462952d822840513675045da",
"classDigikam_1_1ImageQualityParser.html#aa081c33790e2bf5b542ca9bdbd90a88c",
"classDigikam_1_1ImageWindow.html#ac5cbb245733f8bde53aeb35e9a595e98",
"classDigikam_1_1ImportDownloadOverlay.html#a1de6488af4cf2cdd8698183d46033705",
"classDigikam_1_1ImportIconView.html#ae8b3760ccc68d3f01a9a158964e6a0b3",
"classDigikam_1_1ImportRotateOverlay.html#a3d1a66c0872b1dc98193c803a8b50fc9",
"classDigikam_1_1ImportUI.html#a39d8f5ce4f58560b307097609f8755d0",
"classDigikam_1_1InvertFilter.html#a901ef9d13271a082592eed6e172a0ca4",
"classDigikam_1_1ItemAlbumFilterModel.html#ac7e3a20ed3d106c1add0399943f41562",
"classDigikam_1_1ItemDelegate.html#ab3718a44f05844634012be81d44331a6",
"classDigikam_1_1ItemFilterModel.html#a1afb1edadb75d2cd064f96039e33fecd",
"classDigikam_1_1ItemFilterModelWorker.html#a828d5aaa125ddbbe18a6b750359ce02e",
"classDigikam_1_1ItemHistoryGraphData.html#a7e50ca80acc5dbda5ea1a765e18a4cb7",
"classDigikam_1_1ItemInfo.html#a0295420642552cb062961902f3b4c4c5",
"classDigikam_1_1ItemListModel.html#a35c4fdabfea0f43373c22606b4eb9df3",
"classDigikam_1_1ItemPosition.html#af51673fed64111a3b01130217e524df5",
"classDigikam_1_1ItemScanner.html#a101864a3c2ed364d4b8f1f360b299311",
"classDigikam_1_1ItemThumbnailDelegate.html#a0e6831addefb2884ce9204b6a89662aa",
"classDigikam_1_1ItemViewImportDelegate.html#a42615afbe40dc888efe2e866eff0d41c",
"classDigikam_1_1LBPHistogramMetadata.html#a7cc41b1ca0f28665e4f6b6e90951c556",
"classDigikam_1_1LightTablePreview.html#a44b7ce7e4b9bec66ca4feafd48b8d1fc",
"classDigikam_1_1LoadingDescription_1_1PreviewParameters.html#a221519f941cf379c7c605445f7659abf",
"classDigikam_1_1ManagedLoadSaveThread.html#a55c3c12f5a5ac1b12151d90271acadc1adf582e3d75b8139e8605b12210bcafc7",
"classDigikam_1_1MetaEngine.html#a8e34eaf5a9f284459f1121a34fedc5d2",
"classDigikam_1_1MixerContainer.html#a03640ef03f31f2ffaa746561155ba0d6",
"classDigikam_1_1NamespaceListView.html#a5a15b8331e4f0dc3eabb667596af80b4",
"classDigikam_1_1OutlookBinary.html#a8458144e4533cd18eb79dddba7080b81",
"classDigikam_1_1PickLabelFilter.html#a605c5fea5b94b3717111608579461ef1",
"classDigikam_1_1ProxyLineEdit.html#a099ea6fafd80c62f535aa70ebdc7a759",
"classDigikam_1_1RatingComboBox.html#a45a4cde07b1e7384a8aa7c36f16f9c58a124d6d6c24e1012228e3bfb372d76476",
"classDigikam_1_1RecognitionDatabase.html#a4a612ae4ae905a6b4f25df662ac8fade",
"classDigikam_1_1Restoration.html#a61b1ca5ab214f7b82356116cdb2b66e1",
"classDigikam_1_1ScanController.html#a53f72741c3781616f586781deb493101",
"classDigikam_1_1SearchFieldAlbum.html#af0943e1b11ed6ed789015d7e8b917214",
"classDigikam_1_1SearchFieldPageOrientation.html#ab55a3e2d7188c11703e21cd7ffd7a5cda25b179a8af9718b2df923a2242204713",
"classDigikam_1_1SearchFilterModel.html#abfdc7abf2196d99cd53d0c2a79efed3a",
"classDigikam_1_1SearchTreeView.html#a6e7b9c3aabb025ec5e988e5064f71945",
"classDigikam_1_1SearchesJob.html#aa34d76e1cd1e839039f1b67add81920d",
"classDigikam_1_1SharedLoadSaveThread.html#afd6fad49958513405fc5b5c060258e36",
"classDigikam_1_1SimilarityDbBackend.html#ac7c479009ec00f4c23bff1023758b72e",
"classDigikam_1_1TAlbum.html#ad7e7318fe819747ec838d25d21a9662c",
"classDigikam_1_1TableViewColumns_1_1ColumnItemProperties.html#ae53896a52f61680c4263e571e19eec7f",
"classDigikam_1_1TagPropertiesFilterModel.html#a0504105eebbf2102c1d0f44fd97320fa",
"classDigikam_1_1TagTreeViewSelectComboBox.html#aed6c1b6fb8d2d223ddcc8518eabdd908",
"classDigikam_1_1TextureFilter.html#a1678d94b251ed38e13be41c56406282f",
"classDigikam_1_1ThumbnailLoadThread.html#a4bb03f16bfec895a92ee70554f514c30",
"classDigikam_1_1ThumbsDbBackend.html#a9acbb2d6be9391b299266c0c0b5dcd8c",
"classDigikam_1_1TrainerWorker.html#a0e4fe71b3aaeaf90979d9c7f19d7b280a985aa0e2416222ee43292f990cb21bce",
"classDigikam_1_1UMSCamera.html#ac17b6f89acae2af40dd415dcdcd8bf98",
"classDigikam_1_1VidSlideSettings.html#a31ffc35f10ef1913cd597697811a3494a8e5da9b732693c86863fc6f8b50df7af",
"classDigikam_1_1WelcomePage.html#a51a515025cb59b7999ce1e3f1730bedd",
"classShowFoto_1_1ItemViewShowfotoDelegate.html#aa66d642e7d391a77a1319244e0cad055",
"classShowFoto_1_1ShowFoto.html#ad15188bb9b23c1cf411064f0f869d933",
"classShowFoto_1_1ShowfotoNormalDelegate.html#a01bc3cd9e551f383945170c504bac13e",
"classYFAuth_1_1vlong.html#a290a53bfd52c20d040c7c95218c28e0f",
"classenc__node.html#a9e7b6dd8250d36cab0601cd3dbfa6b29",
"classheif_1_1Box__clap.html#adecf2125f8dba3b2d1d369c9318c8122",
"classheif_1_1Box__idat.html#a7abc183ab5226b5771ab9db93082508a",
"classheif_1_1Box__iprp.html#ad9050dc3e81b9a63c656753ff83b9a56",
"classheif_1_1Context.html#a86fd1a9afba51e60aa3e3c2492b15100",
"classheif_1_1ImageHandle.html#a8bc2dad96ba8a332e79e49162ea4ee7f",
"classoption__ALGO__TB__IntraPredMode__Subset.html#a5017daaf83a4093fa91898a12621ca91",
"classoption__PartMode.html#af9d5bea8ee1e6774175894e8fac88ec6",
"classpps__range__extension.html#a6bce3d8553e7adad7b49be27a41bc899",
"classsmall__image__buffer.html#a33f972d4c9a24b1d02a2d9ebb0b3e097",
"functions_enum_r.html",
"namespaceDigikamEditorAdjustLevelsToolPlugin.html",
"namespacemembers_eval_p.html",
"structDigikam_1_1PTOType_1_1Image.html#a8a2be15c2267ae595f57fb61699b7892",
"structde265__image.html#a1039f0b336c5c76cedb72e9e9407c283",
"structheif__color__profile__nclx.html#a3f291960210c8d2017a4cd01bc687108",
"structpt__script__pano.html#a157580b9c401f0ee543e79bb034c72cc"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';