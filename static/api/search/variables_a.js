var searchData=
[
  ['jpeg2000compression',['JPEG2000Compression',['../classDigikam_1_1IOFileSettings.html#adcb31b2b95b0112627e393a956b24e73',1,'Digikam::IOFileSettings']]],
  ['jpeg2000lossless',['JPEG2000LossLess',['../classDigikam_1_1IOFileSettings.html#a425d2924e72c2d8e360e0943c985cce3',1,'Digikam::IOFileSettings']]],
  ['jpegcompression',['JPEGCompression',['../classDigikam_1_1IOFileSettings.html#aa7e24c58678e8df278bfd84cf23b2c78',1,'Digikam::IOFileSettings']]],
  ['jpeglosslesscompression',['jpegLossLessCompression',['../classDigikam_1_1DNGWriter.html#ab676fc9e2bf50fe8c799c3da3ba64e3c',1,'Digikam::DNGWriter']]],
  ['jpegsubsampling',['JPEGSubSampling',['../classDigikam_1_1IOFileSettings.html#ad734d72f8cb98d780ac0c76f43721bba',1,'Digikam::IOFileSettings']]],
  ['jsonfilepath',['jsonFilePath',['../classDigikam_1_1DTrashItemInfo.html#abe09e4459779c8f6c52f5afb84328308',1,'Digikam::DTrashItemInfo']]]
];
