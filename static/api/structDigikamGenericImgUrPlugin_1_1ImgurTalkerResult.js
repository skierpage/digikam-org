var structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult =
[
    [ "ImgurAccount", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurAccount.html", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurAccount" ],
    [ "ImgurImage", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage.html", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult_1_1ImgurImage" ],
    [ "account", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult.html#ae6befbda63ef7fc74fb40f6968a50bbd", null ],
    [ "action", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult.html#a51c578ec12db01c44acbbad27b80cfd6", null ],
    [ "image", "structDigikamGenericImgUrPlugin_1_1ImgurTalkerResult.html#a79cbc4ae9938228a30269c334058a9bd", null ]
];