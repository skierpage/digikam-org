var structDigikamGenericPanoramaPlugin_1_1PanoActionData =
[
    [ "PanoActionData", "structDigikamGenericPanoramaPlugin_1_1PanoActionData.html#a0dac29848a1f88da5b62b274d1a89b25", null ],
    [ "action", "structDigikamGenericPanoramaPlugin_1_1PanoActionData.html#aacc3e3700898731565599f9fd6348758", null ],
    [ "id", "structDigikamGenericPanoramaPlugin_1_1PanoActionData.html#a57c5761410727ebfd4207ffeabc7bdf8", null ],
    [ "message", "structDigikamGenericPanoramaPlugin_1_1PanoActionData.html#a89abf210426128989db33b2b0bf051af", null ],
    [ "starting", "structDigikamGenericPanoramaPlugin_1_1PanoActionData.html#abe02bc3e1cd1e05a6bdf81b3d28aa481", null ],
    [ "success", "structDigikamGenericPanoramaPlugin_1_1PanoActionData.html#ab206f31d42bd7b7964c0802c44e5048b", null ]
];